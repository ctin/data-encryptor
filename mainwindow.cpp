#include "mainwindow.h"
#include <QDrag>
#include <QMimeData>
#include <QXmlStreamReader>
#include <QFileDialog>
#include <QDesktopWidget>
#include <QMenu>
#include <QMessageBox>
#include <QClipboard>
#include <QCompleter>

#include "keyCatchers.h"
#ifdef Q_OS_WIN
#include <windows.h>
#endif //Q_OS_WIN
#include "shellextensionmanager.h"

class TabKeyCatcher;
class DelTabKeyCatcher;
class DownTabKeyCatcher;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), m_windowHeight(292), m_fileWidgetHeight(280)
{
    m_pDialogSelectUser = new DialogSelectUser();
    //connect(m_pDialogSelectUser, SIGNAL(accepted()), this, SLOT(onDialogSelectUserAccepted()));
    m_pDialogAbout = new DialogAbout(this);
    setupUi(this);
    comboBox_name->lineEdit()->setPlaceholderText(tr("Выберите адресата"));
#ifdef Q_OS_WIN
    pushButton_connect->setFlat(true);
    pushButton_connect->setEnabled(false);
    //pushButton_connect->setMinimumHeight(pushButton_selectUser->height());
#else
    pushButton_connect->setCursor(Qt::PointingHandCursor);
#endif //Q_OS_WIN

    plainTextEdit_original->setTabChangesFocus(true);
    plainTextEdit_transformed->setFocusPolicy(Qt::StrongFocus);
    TabKeyCatcher *pKeyCatcher = new DelTabKeyCatcher(this);
    connect(pKeyCatcher, SIGNAL(tabPressed()), this, SLOT(focusToNext()));
    connect(pKeyCatcher, SIGNAL(backtabPressed()), this, SLOT(focusToPrev()));
    connect(pKeyCatcher, SIGNAL(delPressed()), pushButton_removeFile, SIGNAL(clicked()));
    tableWidget->installEventFilter(pKeyCatcher);

    plainTextEdit_original->setAcceptDrops(false);
    tableWidget->setAcceptDrops(false);
    plainTextEdit_transformed->setAcceptDrops(false);
    this->setAcceptDrops(true);
    centralWidget1->setLayout(verticalLayout_2);
    widget_fileArea->setFixedHeight(m_fileWidgetHeight);
    widget_fileArea->hide();

    setFixedHeight(m_windowHeight);

    tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Interactive);
    m_pSignalMapper = new QSignalMapper(this);
    connect(m_pSignalMapper, SIGNAL(mapped(QString)), this, SIGNAL(changeLanguage(QString)));
#ifndef QT_NO_SYSTEMTRAYICON
    createActions();
    createTrayIcon();

    connect(m_pTrayIcon, SIGNAL(messageClicked()), this, SLOT(messageClicked()));
    connect(m_pTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    m_pTrayIcon->show();
#endif //QT_NO_SYSTEMTRAYICON
    setDeviceOpened(false);
    retranslateThis();

#ifdef Q_OS_WIN
    if(!ShellExtensionManager::checkAdminRights())
    {
        m_pRegAction->setEnabled(false);
        m_pUnregAction->setEnabled(false);
    }
#endif //Q_OS_WIN

    connect(QApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(clipboardDataChanged()));
    clipboardDataChanged();
    on_plainTextEdit_original_textChanged();
    on_plainTextEdit_transformed_textChanged();
}


void MainWindow::translate(QString arg)
{
    if(arg.startsWith("ru"))
        m_pRusLanguage->setChecked(true);
    else if(arg.startsWith("en"))
        m_pEnLanguage->setChecked(true);
    m_pDialogSelectUser->retranslateUi(m_pDialogSelectUser);
    retranslateThis();
    retranslateUi(this);
    m_pDialogAbout->refreshText();
}

void MainWindow::updateNames(const QStringList &names)
{
    comboBox_name->addItems(names);
    comboBox_name->setCurrentIndex(-1);
    comboBox_name->completer()->setCompletionMode(QCompleter::PopupCompletion);
    comboBox_name->completer()->setFilterMode(Qt::MatchContains);
}

#ifndef QT_NO_SYSTEMTRAYICON
void MainWindow::createTrayIcon()
{
    m_pTrayIconMenu = new QMenu(this);
    m_pTrayIconMenu->addAction(m_pShowAbout);
    m_pTrayIconMenu->addSeparator();
    QActionGroup *pActionGroup = new QActionGroup(this);
    pActionGroup->addAction(m_pRusLanguage);
    pActionGroup->addAction(m_pEnLanguage);
    m_pTrayIconMenu->addAction(m_pRusLanguage);
    m_pTrayIconMenu->addAction(m_pEnLanguage);
    m_pTrayIconMenu->addSeparator();

#ifdef Q_OS_WIN
    m_pTrayIconMenu->addAction(m_pRegAction);
    m_pTrayIconMenu->addAction(m_pUnregAction);
    m_pTrayIconMenu->addSeparator();
#endif //Q_OS_WIN
    m_pTrayIconMenu->addAction(m_pMinimizeAction);
    m_pTrayIconMenu->addAction(m_pRestoreAction);
    m_pTrayIconMenu->addSeparator();
    m_pTrayIconMenu->addAction(m_pQuitAction);

    m_pTrayIcon = new QSystemTrayIcon(this);
    m_pTrayIcon->setContextMenu(m_pTrayIconMenu);
    m_pTrayIcon->setIcon(QIcon(":/Images/connect.png"));
}

void MainWindow::retranslateThis()
{
    m_pShowAbout->setText(tr("О программе"));
    m_pMinimizeAction->setText(tr("Свернуть"));
    m_pRestoreAction->setText(tr("Развернуть"));
    m_pQuitAction->setText(tr("Выйти"));
    comboBox_name->lineEdit()->setPlaceholderText(tr("Выберите адресата"));
#ifdef Q_OS_WIN
    m_pRegAction->setText(tr("Зарегистрировать в контекстном меню"));
    m_pUnregAction->setText(tr("Снять регистрацию в контекстном меню"));
#endif //Q_OS_WIN
}

void MainWindow::createActions()
{
    m_pShowAbout = new QAction(this);
    connect(m_pShowAbout, SIGNAL(triggered()), m_pDialogAbout, SLOT(show()));

    m_pRusLanguage = new QAction("Русский", this);
    m_pRusLanguage->setCheckable(true);
    connect(m_pRusLanguage, SIGNAL(triggered()), m_pSignalMapper, SLOT(map()));
    m_pSignalMapper->setMapping(m_pRusLanguage, "ru");

    m_pEnLanguage = new QAction("English", this);
    m_pEnLanguage->setCheckable(true);
    connect(m_pEnLanguage, SIGNAL(triggered()), m_pSignalMapper, SLOT(map()));
    m_pSignalMapper->setMapping(m_pEnLanguage, "en");

    m_pMinimizeAction = new QAction(this);
    connect(m_pMinimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    m_pRestoreAction = new QAction(this);
    connect(m_pRestoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    m_pQuitAction = new QAction(this);
    connect(m_pQuitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

#ifdef Q_OS_WIN
    m_pRegAction = new QAction(this);
    connect(m_pRegAction, SIGNAL(triggered()), this, SIGNAL(registerShellExtension()));

    m_pUnregAction = new QAction(this);
    connect(m_pUnregAction, SIGNAL(triggered()), this, SIGNAL(unregisterShellExtension()));
#endif //Q_OS_WIN
}

void MainWindow::showMessage(QString message)
{
    m_pTrayIcon->showMessage(tr("Шифратор данных"), message, QSystemTrayIcon::Information,
                          5000);
}

void MainWindow::forceShowOverAll()
{
#ifdef Q_OS_WIN
        ::ShowWindow((HWND)winId(), SW_SHOWNORMAL);
        ::SetWindowPos((HWND)winId(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        ::SetWindowPos((HWND)winId(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif // Q_OS_WIN
    showNormal();
    activateWindow();
    raise();
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        if(isMinimized())
            forceShowOverAll();
        break;
    case QSystemTrayIcon::DoubleClick:
        forceShowOverAll();
        break;
    case QSystemTrayIcon::MiddleClick:
        break;
    default:
        ;
    }
}

void MainWindow::messageClicked()
{
    /*QMessageBox::information(0, tr("Systray"),
                             tr("Sorry, I already gave what help I could.\n"
                                "Maybe you should try asking a human?"));*/
}

#endif //QT_NO_SYSTEMTRAYICON

void MainWindow::focusToNext()
{
    focusNextChild();
}

void MainWindow::focusToPrev()
{
    focusPreviousChild();
}

void MainWindow::onDialogSelectUserAccepted()
{
    //if(m_pDialogSelectUser->listWidget->currentItem())
    //lineEdit_name->setText(m_pDialogSelectUser->listWidget->currentItem()->text());
}

void MainWindow::clipboardDataChanged()
{
    pushButton_paste_original->setEnabled(!QApplication::clipboard()->text().isEmpty());
}

void MainWindow::on_pushButton_showHideFileArea_toggled(bool checked)
{
    pushButton_showHideFileArea->setIcon(QIcon(checked ? ":/Images/arrow-expanded.png" : ":/Images/arrow.png"));

    setFixedHeight(checked ? m_windowHeight + m_fileWidgetHeight + verticalLayout_2->spacing() : m_windowHeight);
    widget_fileArea->setVisible(checked);
    QDesktopWidget widget;
    int diff = widget.availableGeometry(this).height() - geometry().bottomLeft().y();
    if(diff < 0)
    {
        move(pos() + QPoint(0, diff));
    }
}

void MainWindow::setDeviceOpened(bool isOpen)
{
    QString imageAddr = QString(":/Images/") + (isOpen ? "" : "dis") + "connect.png";
    QIcon icon(imageAddr);
#ifdef Q_OS_WIN
    pushButton_connect->setIcon(icon);
#endif //Q_OS_WIN
#ifndef QT_NO_SYSTEMTRAYICON
    m_pTrayIcon->setIcon(icon);
#endif //QT_NO_SYSTEMTRAYICON

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    emit beforeClose();
    m_pTrayIcon->hide();
    qApp->quit();
}

void MainWindow::changeEvent(QEvent *event)
{
    QMainWindow::changeEvent(event);
    if(event->type() == QEvent::WindowStateChange)
    {
        if(isMinimized())
        {
            hide();
            event->ignore();
        }
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasColor() || event->mimeData()->hasImage())
        return;
    event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasUrls())
    {
        foreach (QUrl url, event->mimeData()->urls()) {
            if(url.isLocalFile())
                emit fileAdded(url.toLocalFile());
        }
    }
    else
    {
        if(event->mimeData()->hasText())
        {
            QString text = event->mimeData()->text();
            if(QFile::exists(text))
                emit fileAdded(text);
            else
                plainTextEdit_original->insertPlainText(text);
        }
        if(event->mimeData()->hasHtml())
        {
            QString textFromHTML;
            QXmlStreamReader xml(event->mimeData()->html());
            while(!xml.atEnd())
            {
                if(xml.readNext() == QXmlStreamReader::Characters)
                {
                    if(!xml.text().contains("p, li { white-space: pre-wrap; }"))
                        textFromHTML += xml.text();
                }
            }
            plainTextEdit_original->insertPlainText(textFromHTML);
        }
    }
}

void MainWindow::on_pushButton_copy_original_clicked()
{
    QApplication::clipboard()->setText(plainTextEdit_original->toPlainText());
    plainTextEdit_original->selectAll();
}

void MainWindow::on_pushButton_copy_transformed_clicked()
{
    QApplication::clipboard()->setText(plainTextEdit_transformed->toPlainText());
    plainTextEdit_transformed->selectAll();
}

void MainWindow::on_plainTextEdit_original_textChanged()
{
    pushButton_copy_original->setEnabled(!plainTextEdit_original->toPlainText().isEmpty());
    pushButton_clear_original->setEnabled(!plainTextEdit_original->toPlainText().isEmpty());
}

void MainWindow::on_pushButton_paste_original_clicked()
{
    plainTextEdit_original->insertPlainText(QApplication::clipboard()->text());
}

void MainWindow::on_pushButton_clear_original_clicked()
{
    plainTextEdit_original->clear();
}

void MainWindow::on_plainTextEdit_transformed_textChanged()
{
    pushButton_copy_transformed->setEnabled(!plainTextEdit_transformed->toPlainText().isEmpty());
}
