#include "application.h"
#include "windows.h"
#include "dbt.h"

AbstractNativeEventFilter::AbstractNativeEventFilter(Application *pApp) : m_pApplication(pApp) {

}

bool AbstractNativeEventFilter::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType);
    Q_UNUSED(result);
    MSG* winMessage = (static_cast<MSG*>(message));
    if(winMessage->message == WM_DEVICECHANGE)
    {
        m_pApplication->onDeviceChanged(2000);
    }
    return false;
}

Application::Application(int &argc, char *argv[]) :
    QApplication(argc, argv)
{
    m_portChangedTimer.setInterval(2000);
    m_portChangedTimer.setSingleShot(true);
    installNativeEventFilter(new AbstractNativeEventFilter(this));
    connect(&m_portChangedTimer, SIGNAL(timeout()), this, SIGNAL(deviceChanged()));
}

void Application::onDeviceChanged(int ms)
{
    m_portChangedTimer.start(ms);
}




