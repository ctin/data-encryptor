<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AllThisThings</name>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="221"/>
        <source>Проверка целостности сообщения</source>
        <translation>Verifying hash</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="502"/>
        <source>Операция отменена.</source>
        <translation>Operation cancelled.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="503"/>
        <source>Не получилось подключиться к шифратору.</source>
        <translation>Failed to connect to encryptor.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="504"/>
        <source>Ошибка перехода шифратора в режим шифрования.</source>
        <translation>Failed to enter encryption mode.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="505"/>
        <source>Потеряна связь с шифратором. Операция выполнена не полностью.</source>
        <translation>Lost connection to encryptor. Operation has not been completed.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="506"/>
        <source>Ошибка в процессе шифрования.</source>
        <translation>Encryption error.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="507"/>
        <source>Ошибка считывания данных для шифрования.</source>
        <translation>Error reading input data.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="508"/>
        <source>Ошибка записи обработанных данных.</source>
        <translation>Error writing output data.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="509"/>
        <source>Ошибка целостности данных.</source>
        <translation>Data corrupted.</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../core.cpp" line="137"/>
        <source>Устройство не обнаружено</source>
        <translation>Device not found</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="140"/>
        <source>Считывается таблица пользователей</source>
        <translation>Reading subscribers list</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="140"/>
        <source>Остановить</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="156"/>
        <source>Устройство Stealthphone %1 подключено</source>
        <translation>Stealthphone %1 connected</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="158"/>
        <source>Невозможно считать список абонентов</source>
        <translation>Cannot read subscribers list</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="208"/>
        <source>Я</source>
        <translation>Me</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="212"/>
        <source>Неизвестно</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="256"/>
        <source>Подключите шифратор</source>
        <translation>Connect encryptor</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="297"/>
        <source>Подключите шифратор, чтобы выполнить зашифрование.</source>
        <translation>Connect encryptor to encrypt message.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="298"/>
        <source>Подключите шифратор, чтобы выполнить расшифрование.</source>
        <translation>Connect encryptor to decrypt message.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="305"/>
        <source>Текст успешно зашифрован</source>
        <translation>Text encrypted successfully</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="305"/>
        <source>Текст успешно расшифрован</source>
        <translation>Text decrypted successfully</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="338"/>
        <source>Текст шифруется...</source>
        <translation>Encrypting message...</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="338"/>
        <location filename="../core.cpp" line="407"/>
        <location filename="../core.cpp" line="490"/>
        <location filename="../core.cpp" line="908"/>
        <source>Отменить</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="353"/>
        <location filename="../core.cpp" line="418"/>
        <source>Текст форматируется...</source>
        <translation>Preparing message...</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="407"/>
        <source>Текст расшифровывается...</source>
        <translation>Decrypting message...</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="469"/>
        <location filename="../core.cpp" line="908"/>
        <source>Шифрование файла</source>
        <translation>Encrypting file</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="490"/>
        <location filename="../core.cpp" line="581"/>
        <source>Расшифрование файла</source>
        <translation>Decrypting file</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="538"/>
        <source>Перезапись файла</source>
        <translation>Rewriting file</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="538"/>
        <source>Выходной файл %1 является распаковываемым архивом. Перезапись невозможна</source>
        <translation>File%1 name matches archive name. Cannot rewrite</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="551"/>
        <source>Перезаписать?</source>
        <translation>Rewrite?</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="551"/>
        <source>Выходной файл %1 уже существует. Перезаписать?</source>
        <translation>Output file %1 already exists. Rewrite?</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="552"/>
        <source>Применить ко всем файлам архива</source>
        <translation>Apply to all files in archive</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="780"/>
        <source>Укажите в какую директорию распаковать архив</source>
        <translation>Select directory to unpack archive</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="816"/>
        <source>Процесс расшифрования файлов завершен успешно</source>
        <translation>Decryption successfully completed</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="818"/>
        <source>Некоторые файлы не расшифрованы</source>
        <translation>Some files were not decrypted</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="820"/>
        <source>Расшифрование не выполнено.</source>
        <translation>Decryption failed.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="870"/>
        <source>Ошибка</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="870"/>
        <source>Нет файлов или папок для шифрования</source>
        <translation>No files or folders to encrypt</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="875"/>
        <location filename="../core.cpp" line="880"/>
        <location filename="../core.cpp" line="886"/>
        <location filename="../core.cpp" line="900"/>
        <location filename="../core.cpp" line="1140"/>
        <source>Ошибка логина</source>
        <translation>Login error</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="891"/>
        <source>Укажите адрес контейнера для сохранения данных</source>
        <translation>Specify a container for storing data</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="893"/>
        <source>Зашифрованные файлы </source>
        <comment>параметр диалога выбора файла</comment>
        <translation>Encrypted files</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="919"/>
        <source>Файл является целевым архивом</source>
        <translation>Cannot rewrite target archive</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="949"/>
        <source>Процесс зашифрования файлов завершен успешно</source>
        <translation>Encryption successfully completed</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="954"/>
        <source>Зашифрование не выполнено</source>
        <translation>Encryption failed.</translation>
    </message>
    <message>
        <source>Укажите файлы</source>
        <translation type="vanished">Select files</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1249"/>
        <source>Невозможно открыть исходный файл.</source>
        <translation>Cannot open input file.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1251"/>
        <source>Невозможно открыть результирующий файл.</source>
        <translation>Cannot open output file.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1259"/>
        <source>Выберите абонента, для которого требуется зашифровать сообщение.</source>
        <translation>Select recipient</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="951"/>
        <source>Некоторые файлы не зашифрованы</source>
        <translation>Some files were not encrypted</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1036"/>
        <source>Выберите папку</source>
        <translation>Select folder</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1043"/>
        <source>Выберите файлы</source>
        <translation>Select files</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1110"/>
        <location filename="../core.cpp" line="1116"/>
        <location filename="../core.cpp" line="1124"/>
        <location filename="../core.cpp" line="1130"/>
        <source>Ошибка файла</source>
        <translation>File error</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1110"/>
        <source>Нет такого входного файла!</source>
        <translation>Input file does not exist!</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1116"/>
        <source>Нельзя открыть входной файл %1</source>
        <translation>Cannot open input file %1</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1124"/>
        <location filename="../core.cpp" line="1130"/>
        <source>Файл для расшифровки имеет неизвестный формат</source>
        <translation>File for decryption has wrong format</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1140"/>
        <source>Указан неверный логин</source>
        <translation>Wrong login</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1248"/>
        <source>Файл не найден.</source>
        <translation>File not found.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1250"/>
        <source>Не получилось создать папку.</source>
        <translation>Cannot create folder.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1252"/>
        <source>Файл зашифрован для другого абонента.</source>
        <translation>File is intended for another subscriber.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1253"/>
        <source>Сообщение предназначено для другого абонента.</source>
        <translation>The message is intended for another subscriber.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1254"/>
        <source>Расшифрование сообщений от этого абонента невозможно.</source>
        <translation>Unknown sender.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1255"/>
        <source>Данные для расшифровки имеют неизвестный формат.</source>
        <translation>Data for decryption has wrong format.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1256"/>
        <source>Ошибка целостности данных</source>
        <translation>Data corrupted</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1257"/>
        <source>Слишком большой текст! Сохраните его в файл и используйте шифрование файла для корректной работы!</source>
        <translation>Text is too large! Save as file and use file encryption!</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1260"/>
        <source>Некорретный адресат. Выберите существующего адресата из списка.</source>
        <translation>Incorrect subscriber name. Select subscriber from the list.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1261"/>
        <source>Устройство не предоставило логин. Требуется переподключение.</source>
        <translation>Cannot read login from device. Reconnect the device.</translation>
    </message>
</context>
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../dialogabout.ui" line="14"/>
        <source>Шифратор данных</source>
        <translation>Data encryptor</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="79"/>
        <source>&lt;a href=&quot;http://www.mttgroup.ch&quot;&gt;http://www.mttgroup.ch&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://www.mttgroup.ch&quot;&gt;http://www.mttgroup.ch&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="97"/>
        <source>&lt;a href=&apos;mailto:info@mttgroup.ch&apos;&gt;info@mttgroup.ch&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;mailto:info@mttgroup.ch&apos;&gt;info@mttgroup.ch&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="112"/>
        <source>tel. +41442103743</source>
        <translation>tel. +41442103743</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="139"/>
        <source>date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="165"/>
        <source>time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="191"/>
        <source>vers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="213"/>
        <source>MTT AG © All rights reserved</source>
        <translation>MTT AG © All rights reserved</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="228"/>
        <source>Switzerlang 8001 Zurich Usterristrasse 11</source>
        <translation>Switzerlang 8001 Zurich Usterristrasse 11</translation>
    </message>
    <message>
        <source>Switzerland 8001 Zurich Usterristrasse 11</source>
        <translation type="vanished">Switzerland 8001 Zurich Usterristrasse 11</translation>
    </message>
    <message>
        <location filename="../dialogabout.cpp" line="31"/>
        <source>Версия программы</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../dialogabout.cpp" line="33"/>
        <source>Дата изготовления</source>
        <translation>Release date</translation>
    </message>
    <message>
        <location filename="../dialogabout.cpp" line="35"/>
        <source>Время изготовления</source>
        <translation>Release time</translation>
    </message>
</context>
<context>
    <name>DialogSelectUser</name>
    <message>
        <location filename="../dialogselectuser.ui" line="14"/>
        <source>Шифратор данных</source>
        <translation>Data encryptor</translation>
    </message>
    <message>
        <location filename="../dialogselectuser.ui" line="47"/>
        <source>Введите имя адресата</source>
        <translation>Select subscriber name</translation>
    </message>
    <message>
        <location filename="../dialogselectuser.ui" line="59"/>
        <source>Выбрать</source>
        <translation>Select</translation>
    </message>
</context>
<context>
    <name>FileWorker</name>
    <message>
        <location filename="../fileworker.cpp" line="38"/>
        <source>Укажите файл</source>
        <translation>Select file</translation>
    </message>
    <message>
        <location filename="../fileworker.cpp" line="170"/>
        <source>Операция успешно завершена</source>
        <translation>Operation successfully completed</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../mainwindow.cpp" line="186"/>
        <source>Шифратор данных</source>
        <translation>Data encryptor</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>Подключите шифратор</source>
        <translation>Connect encryptor</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <source>Кому:</source>
        <translation>To:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <location filename="../mainwindow.ui" line="217"/>
        <source>Копировать всё</source>
        <translation>Copy all</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <source>Вставить текст из буфера обмена</source>
        <translation>Paste</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="180"/>
        <source>Очистить поле ввода текста</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Введите или скопируйте текст сюда</source>
        <translation>Input or paste text</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="274"/>
        <source>Обработанный текст</source>
        <translation>Output text</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Файлы</source>
        <translation>Files</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="354"/>
        <source>Имя файла</source>
        <translation>File name</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="359"/>
        <source>Отправитель</source>
        <translation>Sender</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="375"/>
        <source>Создать криптоархив</source>
        <translation>Create cryptoarchive</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="391"/>
        <source>Расшифровать все</source>
        <translation>Decrypt all</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="412"/>
        <source>Добавить файл</source>
        <translation>Add file</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="441"/>
        <source>Добавить папку</source>
        <translation>Add folder</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="470"/>
        <source>Очистить выбранные строки</source>
        <translation>Clear selected rows</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="499"/>
        <source>Очистить таблицу</source>
        <translation>Clear table</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="30"/>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>Выберите адресата</source>
        <translation>Select subscriber</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="139"/>
        <source>О программе</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="140"/>
        <source>Свернуть</source>
        <translation>Minimize to tray</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <source>Развернуть</source>
        <translation>Maximize</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="142"/>
        <source>Выйти</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="145"/>
        <source>Зарегистрировать в контекстном меню</source>
        <translation>Register program in context menu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="146"/>
        <source>Снять регистрацию в контекстном меню</source>
        <translation>Unregister program in context menu</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="14"/>
        <source>Требуется пароль</source>
        <translation>Enter password</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="14"/>
        <source>Пожалуйста, введите пароль пользователя, чтобы программа могла настроить USB драйвер</source>
        <translation>Programm will configure USB driver. Enter user password, please</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="29"/>
        <location filename="../CryptexUSB/usb_device.cpp" line="37"/>
        <source>Ошибка настройки драйвера</source>
        <translation>Driver configuration error</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="29"/>
        <location filename="../CryptexUSB/usb_device.cpp" line="37"/>
        <source>При настройке драйвера произошла ошибка. Программа не будет запущена.</source>
        <translation>Error occured during driver configuration, programm will not start.</translation>
    </message>
</context>
<context>
    <name>ShellExtensionManager</name>
    <message>
        <location filename="../shellextensionmanager.cpp" line="52"/>
        <location filename="../shellextensionmanager.cpp" line="66"/>
        <location filename="../shellextensionmanager.cpp" line="75"/>
        <source>Использовать шифратор</source>
        <translation>Use encryptor</translation>
    </message>
</context>
</TS>
