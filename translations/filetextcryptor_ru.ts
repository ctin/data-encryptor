<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AllThisThings</name>
    <message>
        <source>Идет проверка целостности сообщения</source>
        <translation type="vanished">Идет проверка целостности сообщения</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="502"/>
        <source>Операция отменена.</source>
        <translation>Операция отменена.</translation>
    </message>
    <message>
        <source>Не получилось подключить шифратор.</source>
        <translation type="vanished">Не получилось подключить шифратор.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="221"/>
        <source>Проверка целостности сообщения</source>
        <translation>Проверка целостности сообщения</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="503"/>
        <source>Не получилось подключиться к шифратору.</source>
        <translation>Не удалось подключиться к шифратору.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="504"/>
        <source>Ошибка перехода шифратора в режим шифрования.</source>
        <translation>Ошибка перехода шифратора в режим шифрования.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="505"/>
        <source>Потеряна связь с шифратором. Операция выполнена не полностью.</source>
        <translation>Потеряна связь с шифратором. Операция выполнена не полностью.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="506"/>
        <source>Ошибка в процессе шифрования.</source>
        <translation>Ошибка в процессе шифрования.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="507"/>
        <source>Ошибка считывания данных для шифрования.</source>
        <translation>Ошибка считывания данных для шифрования.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="508"/>
        <source>Ошибка записи обработанных данных.</source>
        <translation>Ошибка записи обработанных данных.</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/allthisthings.cpp" line="509"/>
        <source>Ошибка целостности данных.</source>
        <translation>Ошибка целостности данных.</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../core.cpp" line="137"/>
        <source>Устройство не обнаружено</source>
        <translation>Устройство не обнаружено</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="140"/>
        <source>Считывается таблица пользователей</source>
        <translation>Считывается таблица пользователей</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="140"/>
        <source>Остановить</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="156"/>
        <source>Устройство Stealthphone %1 подключено</source>
        <translation>Устройство Stealthphone %1 подключено</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="158"/>
        <source>Невозможно считать список абонентов</source>
        <translation>Невозможно считать список абонентов</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="208"/>
        <source>Я</source>
        <translation>Я</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="212"/>
        <source>Неизвестно</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="256"/>
        <source>Подключите шифратор</source>
        <translation>Подключите шифратор</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="297"/>
        <source>Подключите шифратор, чтобы выполнить зашифрование.</source>
        <translation>Подключите шифратор, чтобы выполнить зашифрование.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="298"/>
        <source>Подключите шифратор, чтобы выполнить расшифрование.</source>
        <translation>Подключите шифратор, чтобы выполнить расшифрование.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="305"/>
        <source>Текст успешно зашифрован</source>
        <translation>Текст успешно зашифрован</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="305"/>
        <source>Текст успешно расшифрован</source>
        <translation>Текст успешно расшифрован</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="338"/>
        <source>Текст шифруется...</source>
        <translation>Текст шифруется...</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="338"/>
        <location filename="../core.cpp" line="407"/>
        <location filename="../core.cpp" line="490"/>
        <location filename="../core.cpp" line="908"/>
        <source>Отменить</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="353"/>
        <location filename="../core.cpp" line="418"/>
        <source>Текст форматируется...</source>
        <translation>Текст форматируется...</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="407"/>
        <source>Текст расшифровывается...</source>
        <translation>Текст расшифровывается...</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="469"/>
        <location filename="../core.cpp" line="908"/>
        <source>Шифрование файла</source>
        <translation>Зашифрование файла</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="490"/>
        <location filename="../core.cpp" line="581"/>
        <source>Расшифрование файла</source>
        <translation>Расшифрование файла</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="538"/>
        <source>Перезапись файла</source>
        <translation>Перезапись файла</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="538"/>
        <source>Выходной файл %1 является распаковываемым архивом. Перезапись невозможна</source>
        <translation>Имя распаковываемого файла %1 и архива совпадают. Перезапись невозможна</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="551"/>
        <source>Перезаписать?</source>
        <translation>Перезаписать?</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="551"/>
        <source>Выходной файл %1 уже существует. Перезаписать?</source>
        <translation>Выходной файл %1 уже существует. Перезаписать?</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="552"/>
        <source>Применить ко всем файлам архива</source>
        <translation>Применить ко всем файлам архива</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="780"/>
        <source>Укажите в какую директорию распаковать архив</source>
        <translation>Укажите директорию для распаковки архива</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="816"/>
        <source>Процесс расшифрования файлов завершен успешно</source>
        <translation>Процесс расшифрования файлов завершен успешно</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="818"/>
        <source>Некоторые файлы не расшифрованы</source>
        <translation>Некоторые файлы не расшифрованы</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="820"/>
        <source>Расшифрование не выполнено.</source>
        <translation>Расшифрование не выполнено.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="870"/>
        <source>Ошибка</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="870"/>
        <source>Нет файлов или папок для шифрования</source>
        <translation>Нет файлов или папок для зашифрования</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="875"/>
        <location filename="../core.cpp" line="880"/>
        <location filename="../core.cpp" line="886"/>
        <location filename="../core.cpp" line="900"/>
        <location filename="../core.cpp" line="1140"/>
        <source>Ошибка логина</source>
        <translation>Ошибка логина</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="891"/>
        <source>Укажите адрес контейнера для сохранения данных</source>
        <translation>Укажите контейнер для сохранения данных</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="893"/>
        <source>Зашифрованные файлы </source>
        <comment>параметр диалога выбора файла</comment>
        <translation>Зашифрованные файлы</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="919"/>
        <source>Файл является целевым архивом</source>
        <translation>Файл является целевым архивом.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="949"/>
        <source>Процесс зашифрования файлов завершен успешно</source>
        <translation>Процесс зашифрования файлов завершен успешно.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="954"/>
        <source>Зашифрование не выполнено</source>
        <translation>Зашифрование не выполнено.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1036"/>
        <source>Выберите папку</source>
        <translation>Выберите папку</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1043"/>
        <source>Выберите файлы</source>
        <translation>Выберите файлы</translation>
    </message>
    <message>
        <source>Укажите папку</source>
        <translation type="vanished">Укажите папку</translation>
    </message>
    <message>
        <source>Укажите файлы</source>
        <translation type="vanished">Укажите файлы</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1249"/>
        <source>Невозможно открыть исходный файл.</source>
        <translation>Невозможно открыть исходный файл.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1251"/>
        <source>Невозможно открыть результирующий файл.</source>
        <translation>Невозможно открыть результирующий файл.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1259"/>
        <source>Выберите абонента, для которого требуется зашифровать сообщение.</source>
        <translation>Выберите абонента, для которого требуется зашифровать сообщение.</translation>
    </message>
    <message>
        <source>Процесс шифрования файлов завершен успешно</source>
        <translation type="vanished">Процесс шифрования файлов завершен успешно</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="951"/>
        <source>Некоторые файлы не зашифрованы</source>
        <translation>Некоторые файлы не зашифрованы</translation>
    </message>
    <message>
        <source>Шифрование не выполнено</source>
        <translation type="vanished">Шифрование не выполнено</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1110"/>
        <location filename="../core.cpp" line="1116"/>
        <location filename="../core.cpp" line="1124"/>
        <location filename="../core.cpp" line="1130"/>
        <source>Ошибка файла</source>
        <translation>Ошибка файла</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1110"/>
        <source>Нет такого входного файла!</source>
        <translation>Нет такого входного файла!</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1116"/>
        <source>Нельзя открыть входной файл %1</source>
        <translation>Нельзя открыть входной файл %1</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1124"/>
        <location filename="../core.cpp" line="1130"/>
        <source>Файл для расшифровки имеет неизвестный формат</source>
        <translation>Файл для расшифровки имеет неизвестный формат</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1140"/>
        <source>Указан неверный логин</source>
        <translation>Указан неверный логин</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1248"/>
        <source>Файл не найден.</source>
        <translation>Файл не найден.</translation>
    </message>
    <message>
        <source>Исходный файл не найден.</source>
        <translation type="vanished">Исходный файл не найден.</translation>
    </message>
    <message>
        <source>Нельзя открыть исходный файл.</source>
        <translation type="vanished">Нельзя открыть исходный файл.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1250"/>
        <source>Не получилось создать папку.</source>
        <translation>Невозможно создать папку.</translation>
    </message>
    <message>
        <source>Нельзя открыть выходной файл.</source>
        <translation type="vanished">Нельзя открыть выходной файл.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1252"/>
        <source>Файл зашифрован для другого абонента.</source>
        <translation>Файл предназначен для другого абонента.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1253"/>
        <source>Сообщение предназначено для другого абонента.</source>
        <translation>Сообщение предназначено для другого абонента.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1254"/>
        <source>Расшифрование сообщений от этого абонента невозможно.</source>
        <translation>Расшифрование сообщений от этого абонента невозможно.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1255"/>
        <source>Данные для расшифровки имеют неизвестный формат.</source>
        <translation>Данные для расшифровки имеют неизвестный формат.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1256"/>
        <source>Ошибка целостности данных</source>
        <translation>Ошибка целостности данных</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1257"/>
        <source>Слишком большой текст! Сохраните его в файл и используйте шифрование файла для корректной работы!</source>
        <translation>Слишком большой текст! Сохраните его в файл и используйте шифрование файла!</translation>
    </message>
    <message>
        <source>Укажите адресата.</source>
        <translation type="vanished">Укажите адресата.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1260"/>
        <source>Некорретный адресат. Выберите существующего адресата из списка.</source>
        <translation>Некорретный адресат. Выберите существующего адресата из списка.</translation>
    </message>
    <message>
        <location filename="../core.cpp" line="1261"/>
        <source>Устройство не предоставило логин. Требуется переподключение.</source>
        <translation>Ошибка чтения логина. Переподключите шифратор.</translation>
    </message>
</context>
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../dialogabout.ui" line="14"/>
        <source>Шифратор данных</source>
        <translation>Шифратор данных</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="79"/>
        <source>&lt;a href=&quot;http://www.mttgroup.ch&quot;&gt;http://www.mttgroup.ch&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://www.mttgroup.ch&quot;&gt;http://www.mttgroup.ch&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="97"/>
        <source>&lt;a href=&apos;mailto:info@mttgroup.ch&apos;&gt;info@mttgroup.ch&lt;/a&gt;</source>
        <translation>&lt;a href=&apos;mailto:info@mttgroup.ch&apos;&gt;info@mttgroup.ch&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="112"/>
        <source>tel. +41442103743</source>
        <translation>tel. +41442103743</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="139"/>
        <source>date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="165"/>
        <source>time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="191"/>
        <source>vers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="213"/>
        <source>MTT AG © All rights reserved</source>
        <translation>MTT AG © Все права защищены</translation>
    </message>
    <message>
        <location filename="../dialogabout.ui" line="228"/>
        <source>Switzerlang 8001 Zurich Usterristrasse 11</source>
        <translation>Switzerlang 8001 Zurich Usterristrasse 11</translation>
    </message>
    <message>
        <location filename="../dialogabout.cpp" line="31"/>
        <source>Версия программы</source>
        <translation>Версия программы</translation>
    </message>
    <message>
        <location filename="../dialogabout.cpp" line="33"/>
        <source>Дата изготовления</source>
        <translation>Дата изготовления</translation>
    </message>
    <message>
        <location filename="../dialogabout.cpp" line="35"/>
        <source>Время изготовления</source>
        <translation>Время изготовления</translation>
    </message>
</context>
<context>
    <name>DialogSelectUser</name>
    <message>
        <location filename="../dialogselectuser.ui" line="14"/>
        <source>Шифратор данных</source>
        <translation>Шифратор данных</translation>
    </message>
    <message>
        <location filename="../dialogselectuser.ui" line="47"/>
        <source>Введите имя адресата</source>
        <translation>Введите имя адресата</translation>
    </message>
    <message>
        <location filename="../dialogselectuser.ui" line="59"/>
        <source>Выбрать</source>
        <translation>Выбрать</translation>
    </message>
</context>
<context>
    <name>FileWorker</name>
    <message>
        <location filename="../fileworker.cpp" line="38"/>
        <source>Укажите файл</source>
        <translation>Укажите файл</translation>
    </message>
    <message>
        <location filename="../fileworker.cpp" line="170"/>
        <source>Операция успешно завершена</source>
        <translation>Операция успешно завершена</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../mainwindow.cpp" line="186"/>
        <source>Шифратор данных</source>
        <translation>Шифратор данных</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>Подключите шифратор</source>
        <translation>Подключите шифратор</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <source>Кому:</source>
        <translation>Кому:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <location filename="../mainwindow.ui" line="217"/>
        <source>Копировать всё</source>
        <translation>Копировать всё</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="160"/>
        <source>Вставить текст из буфера обмена</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="180"/>
        <source>Очистить поле ввода текста</source>
        <translation>Очистить поле ввода текста</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Введите или скопируйте текст сюда</source>
        <translation>Введите текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="274"/>
        <source>Обработанный текст</source>
        <translation>Обработанный текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <source>Файлы</source>
        <translation>Файлы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="354"/>
        <source>Имя файла</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="359"/>
        <source>Отправитель</source>
        <translation>Отправитель</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="375"/>
        <source>Создать криптоархив</source>
        <translation>Создать криптоархив</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="391"/>
        <source>Расшифровать все</source>
        <translation>Расшифровать все</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="412"/>
        <source>Добавить файл</source>
        <translation>Добавить файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="441"/>
        <source>Добавить папку</source>
        <translation>Добавить папку</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="470"/>
        <source>Очистить выбранные строки</source>
        <translation>Очистить выбранные строки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="499"/>
        <source>Очистить таблицу</source>
        <translation>Очистить таблицу</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="30"/>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>Выберите адресата</source>
        <translation>Выберите адресата</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="139"/>
        <source>О программе</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="140"/>
        <source>Свернуть</source>
        <translation>Свернуть</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <source>Развернуть</source>
        <translation>Развернуть</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="142"/>
        <source>Выйти</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="145"/>
        <source>Зарегистрировать в контекстном меню</source>
        <translation>Зарегистрировать в контекстном меню</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="146"/>
        <source>Снять регистрацию в контекстном меню</source>
        <translation>Снять регистрацию в контекстном меню</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="14"/>
        <source>Требуется пароль</source>
        <translation>Требуется пароль</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="14"/>
        <source>Пожалуйста, введите пароль пользователя, чтобы программа могла настроить USB драйвер</source>
        <translation>Пожалуйста, введите пароль пользователя, чтобы программа могла настроить USB драйвер</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="29"/>
        <location filename="../CryptexUSB/usb_device.cpp" line="37"/>
        <source>Ошибка настройки драйвера</source>
        <translation>Ошибка настройки драйвера</translation>
    </message>
    <message>
        <location filename="../CryptexUSB/usb_device.cpp" line="29"/>
        <location filename="../CryptexUSB/usb_device.cpp" line="37"/>
        <source>При настройке драйвера произошла ошибка. Программа не будет запущена.</source>
        <translation>При настройке драйвера произошла ошибка. Программа не будет запущена.</translation>
    </message>
</context>
<context>
    <name>ShellExtensionManager</name>
    <message>
        <location filename="../shellextensionmanager.cpp" line="52"/>
        <location filename="../shellextensionmanager.cpp" line="66"/>
        <location filename="../shellextensionmanager.cpp" line="75"/>
        <source>Использовать шифратор</source>
        <translation>Использовать шифратор</translation>
    </message>
</context>
</TS>
