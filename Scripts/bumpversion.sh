#!/usr/bin/env bash

DIR="$( cd "$( dirname "$( dirname "${BASH_SOURCE[0]}" )" )" && pwd )"
VFILE=$DIR/version.h
export GIT_DIR=$DIR/.git

cd $DIR

git checkout develop
git pull
git fetch --tags

VER=`git describe --match "v[0-9]*"`

GITLOG=`git log -n 1 --oneline --format=%s`
if [[ $GITLOG != Version\ Bump\:* ]];
then

  echo "#ifndef __VERSION_H__" > $VFILE
  echo "#define __VERSION_H__" >> $VFILE
  echo "#define VERSION_STRING \"$VER\"" >> $VFILE
  echo "#endif" >> $VFILE

  git add $VFILE
  git commit -m "Version Bump: $VER"
  git push

else

  echo Last commit is version bump. Skipping.

fi
