#include "core.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#ifdef Q_OS_WIN
#include "singleapplication.h"
#include "application.h"
#endif //Q_OS_WIN

int main(int argc, char *argv[])
{
#ifdef Q_OS_WIN
    Application a(argc, argv);
#else
    QApplication a(argc, argv);
#endif //Q_OS_WIN
    a.setQuitOnLastWindowClosed(false);
    a.setOrganizationDomain("http://www.ancort.ru/");
    a.setOrganizationName("ancort");
    a.setApplicationName(TARGET);
    a.setApplicationVersion("0.1");

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("CP1251"));

#ifdef Q_OS_WIN
#ifdef SERIAL
    QString key = "FileTextCryptorKey_serial";
#else
    QString key = "FileTextCryptorKey";
#endif //SERIAL
    SingleApplication wrapper(key);
    if(argc <= 1)
        wrapper.sendMessage("--opened");
    if(wrapper.alreadyExists()){
        for(int i = 1;  i < argc;  i++)
            wrapper.sendMessage(QTextCodec::codecForName("Windows-1251")->toUnicode(argv[i]));

        return 0;
    }
#endif //Q_OS_WIN

    Core c;

#ifdef Q_OS_WIN
    a.connect(&a, SIGNAL(deviceChanged()), &c, SLOT(checkAvailable()));
    a.connect(&wrapper,SIGNAL(messageAvailable(QStringList)),&c,SLOT(receiveMessageFromOtherInstance(QStringList)));
#endif //Q_OS_WIN
    for(int i = 1;  i < argc;  i++)
        c.addFile(QTextCodec::codecForName("Windows-1251")->toUnicode(argv[i]));
    return a.exec();
}
