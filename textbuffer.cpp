#include "textbuffer.h"
#include <QVarLengthArray>

TextBuffer::TextBuffer(const int part, QObject *parent) :
    QIODevice(parent), m_part(part), m_basedPart(QByteArray(m_part, '\0').toBase64().size() + 1)
{
    setEncrypt(false);
}

qint64 TextBuffer::writeData(const char *data, qint64 len)
{
    QByteArray res(data, len);
    if(encrypt())
        emit cryptedTextReady(QString::fromUtf8(res.toBase64()));
    else
        emit textReady(QString::fromUtf8(res));
    return len;
}

qint64 TextBuffer::readData(char *data, qint64 maxlen)
{
    if(maxlen <= 0 || maxlen > m_part)
        return 0;
    if(encrypt())
        return m_buffer.read(data, maxlen);
    else
    {
        const int partSize = (maxlen == m_part) ? m_basedPart : (QByteArray(maxlen, '\0').toBase64().size() + 1);
        QVarLengthArray<char> buffer(partSize);
        int read = m_buffer.read(buffer.data(), partSize);
        if(read <= 0)
            return read;
        QByteArray based = QByteArray::fromBase64(QByteArray(buffer.data(), read));
        int size = qMin(static_cast<qint64>(based.size()), maxlen);
        memcpy(data, based.data(), size);
        return size;
    }
}

void TextBuffer::setBuffer(const QString &text)
{
    m_buffer.close();
    m_buffer.setData(text.toUtf8());
    m_buffer.open(QIODevice::ReadOnly);
}

bool TextBuffer::reset()
{
    return m_buffer.reset();
}

qint64 TextBuffer::size() const
{
    return m_buffer.size();
}

qint64 TextBuffer::bytesAvailable() const
{
    return m_buffer.bytesAvailable();
}

bool TextBuffer::encrypt() const
{
    return m_encrypt;
}

void TextBuffer::setEncrypt(bool arg)
{
    if (m_encrypt == arg)
        return;

    m_encrypt = arg;
    emit encryptChanged(arg);
}
