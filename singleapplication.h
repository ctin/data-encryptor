// "single_application.h"

#ifndef SINGLE_APPLICATION_H
#define SINGLE_APPLICATION_H

#include <QApplication>
#include <QSharedMemory>
#include <QStringList>

class SingleApplication : public QObject
{
        Q_OBJECT
public:
        SingleApplication(const QString uniqueKey);

        bool alreadyExists() const{
            return m_alreadyExists;
        }
        bool isMasterApp() const{
            return !alreadyExists();
        }

        bool sendMessage(const QString &message);
        inline int maxBufferSize();
public slots:
        void checkForMessage();

signals:
        void messageAvailable(const QStringList& messages);

private:
        bool m_alreadyExists;
        QSharedMemory m_sharedMemory;
};
#endif // SINGLE_APPLICATION_H
