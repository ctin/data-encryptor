#include "core.h"
#include <QtCore>
#include <QtWidgets>
#include <limits.h>

#include "CryptexUSB/cryptexlingvo.h"

class MyProgressDialog : public QProgressDialog
{
public:
    MyProgressDialog( const QString & labelText, const QString & cancelButtonText, int minimum, int maximum, QWidget * parent = 0, Qt::WindowFlags flags = 0 )
        : QProgressDialog(  labelText, cancelButtonText, minimum, maximum, parent, flags)
    {
        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
        if(parent)
        {
            setWindowTitle(parent->windowTitle());
            connect(parent, SIGNAL(windowTitleChanged(QString)), this, SLOT(setWindowTitle(QString)));
        }
        setWindowModality(Qt::WindowModal);
        setAutoClose(false);
        setAutoReset(false);
        setEnabled(true);
    }
    void setFormattingState(QString labelText)
    {
        setLabelText(labelText);
        setCancelButton(0);
        setMaximum(0);
        setMinimum(0);
    }
};

static const QString keyName = "username";

using namespace std;

Core::Core(QObject *parent) :
    QObject(parent), m_senderNamePrefix("senderName="), m_receiverNamePrefix("receiverName=")
{
    m_pLanguageChooser = new LanguageChooser(this);
    m_mainWindow.show();

    m_pTimerTextEdit = new QTimer(this);
    m_pTimerTextEdit->setSingleShot(true);
    m_pTimerTextEdit->setInterval(300);

    connect(this, SIGNAL(currentNameChanged(QString)), this, SLOT(recryptInputText()));
    connect(&m_device, SIGNAL(availableChanged(bool)), this, SLOT(updateControlsEnable()));
    connect(&m_device, SIGNAL(serialChanged(QString)), this, SLOT(updateControlsEnable()));
    connect(&m_device, SIGNAL(myLoginChanged(QByteArray)), this, SLOT(onMyLoginChanged(QByteArray)));
    connect(&m_mainWindow, SIGNAL(changeLanguage(QString)), m_pLanguageChooser, SLOT(setLanguage(QString)));
    connect(m_pLanguageChooser, SIGNAL(languageChanged(QString)), &m_mainWindow, SLOT(translate(QString)));
    connect(m_pLanguageChooser, SIGNAL(languageChanged(QString)), this, SLOT(onTranslate()));
    connect(m_pTimerTextEdit, SIGNAL(timeout()), this, SLOT(recryptInputText()));
    connect(&m_mainWindow, SIGNAL(registerShellExtension()), &m_shellExtensionManager, SLOT(registerShellExtension()));
    connect(&m_mainWindow, SIGNAL(unregisterShellExtension()), &m_shellExtensionManager, SLOT(unregisterShellExtension()));
#ifndef Q_OS_WIN
    connect(m_mainWindow.pushButton_connect, SIGNAL(clicked()), this, SLOT(checkAvailable()));
#endif //Q_OS_WIN
    connect(m_mainWindow.pushButton_addFile, SIGNAL(clicked()), this, SLOT(addFile()));
    connect(m_mainWindow.pushButton_addDir, SIGNAL(clicked()), this, SLOT(addDir()));
    connect(m_mainWindow.pushButton_removeFile, SIGNAL(clicked()), this, SLOT(removeSelectedFilesFromTable()));
    connect(m_mainWindow.pushButton_clearTable, SIGNAL(clicked()), this, SLOT(clearTable()));
    connect(m_mainWindow.pushButton_createArchive, SIGNAL(clicked()), this, SLOT(encryptAllFiles()));
    connect(m_mainWindow.pushButton_decryptAll, SIGNAL(clicked()), this, SLOT(decryptAllFiles()));
    connect(&m_mainWindow, SIGNAL(fileAdded(QString)), this, SLOT(addFile(QString)));
    connect(m_mainWindow.comboBox_name, SIGNAL(currentTextChanged(QString)), this, SLOT(onNameChanged(QString)));
    connect(m_mainWindow.tableWidget, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(onTableCellDoubleClicked(int,int)));
    connect(m_mainWindow.tableWidget, SIGNAL(itemSelectionChanged()), this, SLOT(onTableSelectionChanged()));
    connect(&m_mainWindow, SIGNAL(beforeClose()), m_pLanguageChooser, SLOT(saveDefLang()), Qt::DirectConnection);
    connect(m_mainWindow.plainTextEdit_original, SIGNAL(textChanged()), this, SLOT(onOriginalTextChanged()));
    m_checkTimer.setInterval(1000);
    connect(&m_device, SIGNAL(availableChanged(bool)), this, SLOT(onDeviceAvailableChanged(bool)));
    connect(&m_checkTimer, SIGNAL(timeout()), &m_device, SLOT(testConnection()));
    m_pLanguageChooser->loadDefLang();
    qApp->processEvents();
    findDeviceAndRefreshUsers();
    updateControlsEnable();
    m_mainWindow.comboBox_name->setFocus();
    if(qApp->arguments().contains("--test")) {
        QPushButton *test = new QPushButton("test");
        m_mainWindow.verticalLayout->addWidget(test);
        connect(test, SIGNAL(clicked()), this, SLOT(test()));
    }
    onTableSelectionChanged();
}

void Core::onNameChanged(QString arg)
{
    updateControlsEnable();
    if(arg.isEmpty())
    {
        setCurrentName("");
        return;
    }
    if(arg.isEmpty())
        return;
    if(!static_cast<QStringList>(m_userlist.keys()).contains(arg, Qt::CaseInsensitive))
        return;
    QStringList filtered = static_cast<QStringList>(m_userlist.keys()).filter(arg, Qt::CaseInsensitive);
    if(filtered.isEmpty())
        return;
    setCurrentName(filtered.first());
}

void Core::onMyLoginChanged(QByteArray arg)
{
    Q_UNUSED(arg);
    for(int i = 0;  i < m_fileList.size();  i++) {
        if(!m_fileList.at(i)->encrypt())
            m_fileList.at(i)->setCompleted(FileWorker::CREATED);
    }
}

void Core::onTableCellDoubleClicked(int row, int column)
{
    Q_UNUSED(column);
    if(m_fileList.at(row)->completed() == FileWorker::SUCCESS)
        showInGraphicalShell(m_fileList.at(row)->outFileName());
}

void Core::onOriginalTextChanged()
{
    int size = m_mainWindow.plainTextEdit_original->toPlainText().size();
    int interval;
    if(size < CryptexDevice::MAX_BLOCK_SIZE)
        interval = 250;
    else if(size < CryptexDevice::MAX_BLOCK_SIZE * 20)
        interval = 500;
    else
        interval = 1000;
    m_pTimerTextEdit->start(interval);
}

bool Core::findDeviceAndRefreshUsers()
{
    m_mainWindow.comboBox_name->clear();
    bool result = m_device.searchDevice();
    if(!result)
    {
        m_mainWindow.showMessage(tr("Устройство не обнаружено"));
        return false;
    }
    MyProgressDialog progress(tr("Считывается таблица пользователей"), tr("Остановить"), 0, 0, &m_mainWindow);
    progress.setMinimumDuration(1000);
    qApp->setOverrideCursor(Qt::WaitCursor);
    qApp->processEvents();
    m_userlist = m_device.getBindingsTable();
    qApp->restoreOverrideCursor();
    qApp->processEvents();
    result = !m_userlist.isEmpty();
    if(result)
    {
        disconnect(m_mainWindow.comboBox_name, SIGNAL(currentTextChanged(QString)), this, SLOT(onNameChanged(QString)));
        m_mainWindow.updateNames(m_userlist.keys());
        connect(m_mainWindow.comboBox_name, SIGNAL(currentTextChanged(QString)), this, SLOT(onNameChanged(QString)));

    }
    if(result)
        m_mainWindow.showMessage(tr("Устройство Stealthphone %1 подключено").arg(m_device.serial()));
    else
        m_mainWindow.showMessage(tr("Невозможно считать список абонентов"));
    foreach (FileWorker *pWorker, m_fileList) {
        refreshSenderName(pWorker);
    }
    updateControlsEnable();
    return result;
}

void Core::checkAvailable()
{
#ifdef Q_OS_WIN
    QStringList ports = Device::getAvailablePorts();
    if(!m_device.systemPath().isEmpty() && !ports.contains(m_device.systemPath()))
#endif //Q_OS_WIN
    {
        m_device.setSystemPath(QString());
        m_device.setSerial("");
        m_userlist.clear();
        m_device.setAvailable(false);
        m_mainWindow.comboBox_name->clear();
        m_mainWindow.plainTextEdit_original->clear();
        m_mainWindow.plainTextEdit_transformed->clear();
        foreach (FileWorker *pWorker, m_fileList) {
            refreshSenderName(pWorker);
        }
        updateControlsEnable();
    }
    if(m_device.inWork())
        return;
    if(m_device.transaction(Lingvo::CMD_ECHO))
        return;

    if(findDeviceAndRefreshUsers())
        recryptInputText();
}

QByteArray Core::getReceiverLogin()
{
    if(!m_userlist.keys().contains(currentName()))
        return "";
    QByteArray login = m_userlist[currentName()];
    while (login.size() < AllThisThings::loginSize())
        login.push_back('\0');
    return login;
}

void Core::translateTableRow(int i)
{
    FileWorker *pWorker = m_fileList.at(i);
    if(pWorker->sender() == "Me")
    {
        m_mainWindow.tableWidget->item(i, 1)->setText(tr("Я"));
    }
    else if(pWorker->sender() == "Unknown")
    {
        m_mainWindow.tableWidget->item(i, 1)->setText(tr("Неизвестно"));
    }
    else
    {
        m_mainWindow.tableWidget->item(i, 1)->setText(pWorker->sender());
    }
}

void Core::translateTable(int pos)
{
    Q_ASSERT(pos < m_mainWindow.tableWidget->rowCount());
    if(pos >= 0)
    {
        translateTableRow(pos);
    }
    else
    {
        for(int i = 0;  i < m_fileList.length();  i++)
        {
            translateTableRow(i);
        }
    }
}

void Core::onDeviceAvailableChanged(bool available)
{
    if(available || !Device::getAvailablePorts().contains(m_device.systemPath()))
    {
        m_checkTimer.stop();
        return;
    }
    if(!m_device.testConnection())
        m_checkTimer.start();
}

void Core::updateControlsEnable()
{
    bool available = m_device.available();
    m_mainWindow.comboBox_name->setEnabled(available);
    m_mainWindow.setDeviceOpened(available);

    bool ready = m_device.available() && m_device.serial().length();
    QString tmpText = ready ? (QString("Stealthphone[") + m_device.serial() + "]")
            : tr("Подключите шифратор");
    m_mainWindow.label_connectedDevice->setText(tmpText);

    QString currentNameText = m_mainWindow.comboBox_name->currentText();
    bool nameChecked = !currentNameText.isEmpty() && static_cast<QStringList>(m_userlist.keys()).contains(currentNameText, Qt::CaseInsensitive);

    bool containsCrypted = false;
    bool containsOthers = false;
    bool hasKnownUser = false;
    for(QList<FileWorker *>::ConstIterator iter = m_fileList.constBegin();
        iter != m_fileList.constEnd();
        ++iter)
    {

        if((*iter)->encrypt())
            containsOthers = true;
        else
        {
            if((*iter)->sender() != "Unknown")
                hasKnownUser = true;
            containsCrypted = true;
        }
    }
    m_mainWindow.pushButton_clearTable->setEnabled(m_mainWindow.tableWidget->rowCount());
    m_mainWindow.pushButton_createArchive->setEnabled(available && nameChecked && (containsOthers || containsCrypted));
    m_mainWindow.pushButton_decryptAll->setEnabled(hasKnownUser && available && containsCrypted);
}

void Core::recryptInputText()
{
    QString text = m_mainWindow.plainTextEdit_original->toPlainText();
    if(text.isEmpty()) {
        m_mainWindow.plainTextEdit_transformed->clear();
        return;
    }
    QByteArray textArray = text.toUtf8();
    bool decrypt = m_device.adjustToSecretMessage(&textArray);
    if(!m_device.available())
    {
        if(!findDeviceAndRefreshUsers())
        {
            m_mainWindow.plainTextEdit_transformed->setPlainText(!decrypt ? tr("Подключите шифратор, чтобы выполнить зашифрование.")
                                                                         : tr("Подключите шифратор, чтобы выполнить расшифрование."));
            return;
        }
    }
    QString result;
    Result coreResult = decrypt ? decryptText(textArray, &result) : encryptText(textArray, &result);
    if(coreResult)
        m_mainWindow.showMessage(!decrypt ? tr("Текст успешно зашифрован") : tr("Текст успешно расшифрован"));

    m_mainWindow.plainTextEdit_transformed->setPlainText(!coreResult
                                                         ? coreResult.errorMessage()
                                                         : result);
}

Core::Result Core::encryptText(QByteArray &in, QString *out)
{
    Q_ASSERT(out);
    Result coreResult;
    if(m_mainWindow.plainTextEdit_original->toPlainText().size() > 1000000)
    {
        return coreResult.setError(Result::TextIsTooLarge);
    }

    if(m_mainWindow.comboBox_name->currentText().isEmpty())
    {
        return coreResult.setError(Result::ReceiverEmpty);
    }

    if(!m_userlist.keys().contains(currentName()))
    {
        return coreResult.setError(Result::ReceiverIncorrect);
    }
    QByteArray receiverLogin = getReceiverLogin();

    if(m_device.myLogin().isEmpty())
    {
        return coreResult.setError(Result::myLoginError);
    }
    QByteArray outArr;
    m_device.setCryptLogin(receiverLogin);
    MyProgressDialog progress(tr("Текст шифруется..."), tr("Отменить"), 0, 0, &m_mainWindow);
    progress.setMinimumDuration(1000);
    QDataStream inStream(in);
    QDataStream outStream(&outArr, QIODevice::WriteOnly);
    outStream.device()->write(m_device.secretMessage() + m_receiverNamePrefix + QByteArray(receiverLogin.data(), AllThisThings::loginSize()) + m_senderNamePrefix + QByteArray(m_device.myLogin().data(), AllThisThings::loginSize()));
    AllThisThings::Result deepResult = m_device.cryptData(&inStream, &outStream, true, &progress);

    if(!deepResult)
        return coreResult.setDeeperError(deepResult.errorMessage());
    quint16 crc16 = qChecksum(outArr.data(), outArr.length());
    QByteArray tmp = QString("%1").arg(crc16, sizeof(quint16) * 2, 16, QChar('0')).toLatin1();
    outArr.append(tmp);
    QByteArray retBased = outArr.toBase64();
    retBased.append('@');
    *out = QString::fromUtf8(retBased);
    progress.setFormattingState(tr("Текст форматируется..."));
    qApp->processEvents();

    return coreResult;
}

Core::Result Core::decryptText(QByteArray &in, QString *out)
{
    Q_ASSERT(out);
    Result coreResult;
    int index = in.indexOf('@');
    if(index == -1)
    {
        return coreResult.setError(Result::HashError);
    }

    in.remove(index, in.length() - index);

    QByteArray data = QByteArray::fromBase64(in);
    bool ok;
    quint16 crc16;
    int len = sizeof(quint16) * 2;
    QByteArray crcData = data.right(len);
    crc16 = crcData.toInt(&ok, 16);
    data.remove(data.length() - len, len);
    if(!ok || crc16 != qChecksum(data.data(), data.length()))
    {
        return coreResult.setError(Result::HashError);
    }

    data.remove(0, m_device.secretMessage().size());
    if(!data.startsWith(m_receiverNamePrefix) || data.size() < m_receiverNamePrefix.size() + AllThisThings::loginSize() * 2 + m_senderNamePrefix.size())
    {
        return coreResult.setError(Result::WrongCryptedDataFormat);
    }
    data.remove(0, m_receiverNamePrefix.size());
    QByteArray receiverLogin(data.data(), AllThisThings::loginSize());
    data.remove(0, AllThisThings::loginSize());
    data.remove(0, m_senderNamePrefix.size());
    QByteArray senderLogin(data.data(), AllThisThings::loginSize());
    data.remove(0, AllThisThings::loginSize());
    if(senderLogin != m_device.myLogin())
    {
        if(receiverLogin != m_device.myLogin())
        {
            return coreResult.setError(Result::LoginNotFoundForText);
        }
        QByteArray shortSender = senderLogin.left(senderLogin.indexOf('\0'));
        if(!m_userlist.values().contains(shortSender))
        {
            return coreResult.setError(Result::SenderUnknown);
        }
    }
    m_device.setCryptLogin(senderLogin == m_device.myLogin() ? receiverLogin : senderLogin);
    MyProgressDialog progress(tr("Текст расшифровывается..."), tr("Отменить"), 0, 0, &m_mainWindow);
    progress.setMinimumDuration(1000);
    QByteArray outArr;
    QDataStream inStream(data);
    QDataStream outStream(&outArr, QIODevice::WriteOnly);
    AllThisThings::Result deepResult = m_device.cryptData(&inStream, &outStream, false, &progress);

    if(!deepResult)
        return coreResult.setDeeperError(deepResult.errorMessage());
    if(!coreResult)
        return coreResult;
    progress.setFormattingState(tr("Текст форматируется..."));
    qApp->processEvents();
    *out = QString::fromUtf8(outArr);
    return coreResult;
}

Core::Result Core::encryptFile(QProgressDialog *pProgress, QString prefix, QString inFileName, QDataStream *pOutStream)
{
    Q_ASSERT(pOutStream);
    Q_ASSERT(pProgress);
    Result coreResult;
    QFile inFile(inFileName);
    qint64 inFileSize;
    QFileInfo fileInfo(inFileName);
    if(fileInfo.isDir())
    {
        inFileSize = 0;
    }
    else
    {
        if(!inFile.exists())
        {
            return coreResult.setError(Result::NoSuchInFile);
        }
        if(!inFile.open(QFile::ReadOnly))
        {
            return coreResult.setError(Result::CanNotOpenInFile);
        }
        inFileSize = inFile.size();
    }
    bool isDir = fileInfo.isDir();
    QByteArray metaInfo;
    QByteArray cryptedMetaInfo;
    QDataStream cryptedInfoStream(&cryptedMetaInfo, QIODevice::WriteOnly);
    QDataStream(&metaInfo, QIODevice::WriteOnly)
            << isDir
            << prefix
            << fileInfo.fileName()
            << m_device.encryptedSize(inFileSize);
    QDataStream metaInfoStream(&metaInfo, QIODevice::ReadOnly);

    AllThisThings::Result result = m_device.cryptData(&metaInfoStream, &cryptedInfoStream, true);
    if(!result)
    {
        return coreResult.setDeeperError(result.errorMessage());
    }
    *pOutStream << cryptedMetaInfo;//неизвестна длина данных да и лень считать

    if(isDir)
        return coreResult;

    pProgress->setLabelText(tr("Шифрование файла") + QString("\n%1").arg(fileInfo.fileName()));
    pProgress->resize(pProgress->sizeHint());

    QDataStream inFileStream(&inFile);
    result = m_device.cryptData(&inFileStream, pOutStream, true, pProgress);
    if(!result)
        return coreResult.setDeeperError(result.errorMessage());
    return coreResult;
}

Core::Result Core::decryptFile(QString currentPath, QDataStream *pInStream)
{
    Q_ASSERT(pInStream);
    bool canRewriteAll = false;
    bool canSkipAll = false;
    bool allSkipped = true;
    AllThisThings::Result result;
    Result coreResult;

    QFileInfo inFileInfo(*reinterpret_cast<QFile*>(pInStream->device()));

    MyProgressDialog progress(tr("Расшифрование файла"), tr("Отменить"), 0, 0, &m_mainWindow);
    progress.setMinimumDuration(0);

    while(pInStream->device()->bytesAvailable() && pInStream->status() == QDataStream::Ok && result) {
        QByteArray metaInfo;
        QByteArray cryptedMetaInfo;
        *pInStream >> cryptedMetaInfo;

        if(cryptedMetaInfo.isEmpty())
        {
            QDataStream::Status status = pInStream->status();
            if(status != QDataStream::ReadPastEnd)
            {
                result.setError(AllThisThings::Result::ReadStreamError);
                return coreResult.setDeeperError(result.errorMessage());
            }
            return coreResult;
        }
        QDataStream inMetaInfoStream(&cryptedMetaInfo, QIODevice::ReadOnly);
        QDataStream outMetaInfoStream(&metaInfo, QIODevice::WriteOnly);
        result = m_device.cryptData(&inMetaInfoStream, &outMetaInfoStream, false);
        if(!result)
        {
            return coreResult.setDeeperError(result.errorMessage());
        }

        QString prefix, inFileName;
        qint64 inFileSize;
        bool isDir;
        QDataStream metaInfoStream(metaInfo);
        metaInfoStream >> isDir >> prefix >> inFileName >> inFileSize;
        QFileInfo outFileInfo(QDir(currentPath + QDir::separator() + prefix), inFileName);
        QDir newDir(currentPath);
        QString resultPath = currentPath + QDir::separator() + prefix;
        if(isDir)
            resultPath += QDir::separator() + inFileName;
        resultPath = QDir::toNativeSeparators(resultPath);
        if(!newDir.mkpath(resultPath))
        {
            return coreResult.setError(Result::CanNotCreatePath);
        }
        if(isDir)
            continue;
        bool skipFile = isDir;
        if(!skipFile)
        {
            if(outFileInfo.absoluteFilePath() == inFileInfo.absoluteFilePath())
            {
                QMessageBox::information(&m_mainWindow, tr("Перезапись файла"), tr("Выходной файл %1 является распаковываемым архивом. Перезапись невозможна").arg(outFileInfo.fileName()));
                skipFile = true;
            }
            if(!canRewriteAll)
            {
                if(QFile::exists(outFileInfo.absoluteFilePath()))
                {
                    if(canSkipAll)
                    {
                        skipFile = true;
                    }
                    else
                    {
                        QMessageBox box(QMessageBox::Question, tr("Перезаписать?"), tr("Выходной файл %1 уже существует. Перезаписать?").arg(outFileInfo.fileName()), QMessageBox::Yes | QMessageBox::No);
                        box.setCheckBox(new QCheckBox(tr("Применить ко всем файлам архива")));
                        int button = box.exec();
                        if(button == QMessageBox::Yes && box.checkBox()->isChecked())
                        {
                            canRewriteAll = true;
                        }
                        else if(button == QMessageBox::No)
                        {
                            skipFile = true;
                            if(box.checkBox()->isChecked())
                                canSkipAll = true;
                        }
                    }
                }
            }
        }
        if(skipFile)
        {
            int skipped = pInStream->skipRawData(inFileSize);
            if(skipped == -1)
            {
                result.setError(AllThisThings::Result::ReadStreamError);
                return coreResult.setDeeperError(result.errorMessage());
            }
            continue;
        }

        allSkipped = false;

        progress.setLabelText(tr("Расшифрование файла") + QString("\n%1").arg(inFileName));
        progress.resize(progress.sizeHint());

        QFile outFile(outFileInfo.absoluteFilePath());
        if(!outFile.open(QFile::WriteOnly | QFile::Truncate))
        {
            return coreResult.setError(Result::CanNotOpenOutFile);
        }
        QDataStream out(&outFile);
        result = m_device.cryptData(pInStream, &out, false, &progress, inFileSize);
        if(!result)
        {
            outFile.remove();
            return coreResult.setDeeperError(result.errorMessage());
        }
    }
    if(allSkipped)
        return coreResult.setError(Result::Skipped);
    return coreResult;
}


void Core::test()
{
    if(getReceiverLogin().isEmpty())
        return;
    const int maxBuffSize = 400 * 1000 * 1000;

    MyProgressDialog progress(QString("Гоняю тестовый буффер"), "Стоп", 0, 0);
    progress.show();
    m_device.setCryptLogin(getReceiverLogin());
    int errorCount = 0;
    QString lastError;
    qsrand(QTime::currentTime().msecsSinceStartOfDay());

    while(!progress.wasCanceled() && m_device.available())
    {
        qsrand(QTime::currentTime().msecsSinceStartOfDay());
        QThread::msleep(qrand() % 2500);
        double ran = (qrand() + 1)/(double)RAND_MAX;
#ifdef _MSC_VER
#undef max
#endif //_MSC_VER
        qint64 currentBuffSize = static_cast<int>(std::numeric_limits<int>::max() *  ran) % maxBuffSize;
        QBuffer inFile;
        QBuffer outFile;
        QBuffer checkFile;
        if(!inFile.open(QIODevice::ReadWrite))
        {
            QMessageBox::critical(qApp->activeWindow(), "file error", QString("can not open inbuff"));
            return;
        }
        if(!outFile.open(QIODevice::ReadWrite))
        {
            QMessageBox::critical(qApp->activeWindow(), "file error", QString("can not open outbuff"));
            return;
        }
        if(!checkFile.open(QIODevice::ReadWrite))
        {
            QMessageBox::critical(qApp->activeWindow(), "file error", QString("can not open cmpBuff"));
            return;
        }
        progress.setLabelText("создаю файл");
        while(inFile.size() < currentBuffSize)
        {
            qApp->processEvents();
            QByteArray dataToWrite;
            while(dataToWrite.size() < 1000000)
            {
                int rand = qrand();
                dataToWrite.append(reinterpret_cast<char*>(&rand), sizeof(rand));
            }
            inFile.write(dataToWrite.left(currentBuffSize - inFile.size()));
        }
        inFile.seek(0);

        {
            QDataStream inStream(&inFile);
            QDataStream outStream(&outFile);
            progress.setLabelText(QString("Идет зашифрование.\nРазмер: %1, Ошибок %2, последняя: %3").arg(currentBuffSize).arg(errorCount).arg(lastError));
            AllThisThings::Result result = m_device.cryptData(&inStream, &outStream, true, &progress);
            if(!result)
            {
                if(result.m_error == AllThisThings::Result::Cancelled)
                    return;
                errorCount++;
                lastError = result.errorMessage();
                qDebug() << QDateTime::currentDateTime().toString() << "\t"
                            << "buff size" << "\t"
                            << currentBuffSize << "\t"
                            << "errors" << "\t"
                            << errorCount << "\t"
                            << lastError << endl;
                continue;
            }

        }
        inFile.seek(0);
        outFile.seek(0);
        {
            QDataStream inStream(&outFile);
            QDataStream outStream(&checkFile);
            progress.setLabelText(QString("Идет расшифрование.\nРазмер: %1, Ошибок %2, последняя: %3").arg(currentBuffSize).arg(errorCount).arg(lastError));
            AllThisThings::Result result = m_device.cryptData(&inStream, &outStream, false, &progress);
            if(!result)
            {
                if(result.m_error == AllThisThings::Result::Cancelled)
                    return;
                errorCount++;
                lastError = result.errorMessage();
                qDebug() << QDateTime::currentDateTime().toString() << "\t"
                            << "buff size" << "\t"
                            << currentBuffSize << "\t"
                            << "errors" << "\t"
                            << errorCount << "\t"
                            << lastError << endl;
                continue;
            }
            checkFile.seek(0);
            inFile.seek(0);
            if(checkFile.readAll() != inFile.readAll())
            {
                errorCount++;
                lastError = "данные не сходятся";
                qDebug() << QDateTime::currentDateTime().toString() << "\t"
                            << "buff size" << "\t"
                            << currentBuffSize << "\t"
                            << "errors" << "\t"
                            << errorCount << "\t"
                            << lastError << endl;
                continue;
            }
        }

    }
    QMessageBox::information(qApp->activeWindow(), "Проверка окончена", QString("Проверка окончена.\nОшибок: %1\nПоследняя:%2").arg(errorCount).arg(lastError));
}

Core::Result Core::prepareDecryptFile(FileWorker *pWorker)
{
    Q_ASSERT(pWorker);
    pWorker->setCompleted(FileWorker::CREATED);
    Result coreResult;

    if( pWorker->senderLogin() != m_device.myLogin())
    {
        if( pWorker->receiverLogin() != m_device.myLogin())
        {
            return coreResult.setError(Result::LoginNotFoundForFile);
        }
        QByteArray shortSender = pWorker->senderLogin().left(pWorker->senderLogin().indexOf('\0'));
        if(!m_userlist.values().contains(shortSender))
        {
            return coreResult.setError(Result::SenderUnknown);
        }
    }
    if(!QFile::exists(pWorker->inFileName()))
    {
        return coreResult.setError(Result::NoSuchInFile);
    }
    QFile inFile(pWorker->inFileName());

    if(!inFile.open(QFile::ReadOnly))
    {
        return coreResult.setError(Result::CanNotOpenInFile);
    }
    QDataStream in(&inFile);
    QByteArray userData;
    in >> userData;
    if(!m_device.startsWithMessage(userData))
    {
        return coreResult.setError(Result::WrongCryptedDataFormat);
    }
    userData = QByteArray::fromBase64(userData).remove(0, m_device.secretMessage().size());
    if(!userData.startsWith(m_receiverNamePrefix) || userData.indexOf(m_senderNamePrefix) != m_receiverNamePrefix.size() + AllThisThings::loginSize())
    {
        return coreResult.setError(Result::WrongCryptedDataFormat);
    }
    userData.remove(0, m_receiverNamePrefix.size());
    QByteArray receiverLogin = QByteArray(userData.data(), AllThisThings::loginSize());
    userData.remove(0, AllThisThings::loginSize());
    userData.remove(0, m_senderNamePrefix.size());
    QByteArray senderLogin = QByteArray(userData.data(), AllThisThings::loginSize());

    QString dir = QFileDialog::getExistingDirectory(&m_mainWindow, tr("Укажите в какую директорию распаковать архив"), pWorker->inFileName());
    if(dir.isEmpty())
    {
        return coreResult.setError(Result::Skipped);
    }
     m_device.setCryptLogin(senderLogin == m_device.myLogin() ? receiverLogin : senderLogin);

    pWorker->setOutFileName(dir);
    coreResult = decryptFile(dir, &in);
    return coreResult;
}

void Core::decryptAllFiles()
{
    bool oneSuccess = false, allSuccess = true;
    for(QList<FileWorker *>::iterator iter = m_fileList.begin();
        iter != m_fileList.end(); iter++)
    {
        FileWorker *pWorker = *iter;
        QFileInfo inFileInfo(pWorker->inFileName());
        if(inFileInfo.isDir() || FileWorker::encrypt(inFileInfo.fileName()))
            continue;
        if(!m_device.available() && !m_device.testConnection())
        {
            break;
        }
        Result coreResult = prepareDecryptFile(pWorker);
        if(coreResult.m_error != Result::Skipped)
        {
            oneSuccess |= coreResult;
            allSuccess &= coreResult;
            pWorker->setErrorReason(coreResult.errorMessage());
            pWorker->setCompleted(coreResult ? FileWorker::SUCCESS : FileWorker::HAS_ERRORS);
        }
    }
    if(allSuccess && oneSuccess)
        m_mainWindow.showMessage(tr("Процесс расшифрования файлов завершен успешно"));
    else if(oneSuccess)
        m_mainWindow.showMessage(tr("Некоторые файлы не расшифрованы"));
    else
        m_mainWindow.showMessage(tr("Расшифрование не выполнено."));
}

Core::Result Core::prepareEncryptFileFolder(QProgressDialog *pProgress, QDataStream *pOutStream, FileWorker *pWorker)
{
    Q_ASSERT(pProgress);
    Q_ASSERT(pOutStream);
    Q_ASSERT(pWorker);
    pWorker->setCompleted(FileWorker::CREATED);
    Result coreResult;
    QFileInfo inFileInfo(pWorker->inFileName());
    if(!QFile::exists(pWorker->inFileName()))
    {
        return coreResult.setError(Result::NoSuchInFile);
    }

    if(inFileInfo.isFile())
    {
        return encryptFile(pProgress, "/", pWorker->inFileName(), pOutStream);
    }
    else if(inFileInfo.isDir())
    {
        QDir dir(pWorker->inFileName());
        QString dirPath = dir.absolutePath();
        QDirIterator iterator(dirPath, QDirIterator::Subdirectories);
        while (iterator.hasNext()) {
            iterator.next();
            const QFileInfo &fileInfo = iterator.fileInfo();
            QString inFile = fileInfo.absoluteFilePath();
            if(static_cast<QFile*>(pOutStream->device())->fileName() == inFile)
                continue;

            if(inFile.length() <= dirPath.length() || fileInfo.fileName() == "." || fileInfo.fileName() == "..")
                continue;
            coreResult = encryptFile(pProgress, fileInfo.absolutePath().remove(0, dirPath.length() - dir.dirName().length()),
                            inFile, pOutStream);
            if(!coreResult)
                return coreResult;

        }
    }

    return coreResult;
}

void Core::encryptAllFiles()
{
    Result coreResult;
    if(m_fileList.isEmpty())
    {
        QMessageBox::warning(&m_mainWindow, tr("Ошибка"), tr("Нет файлов или папок для шифрования"));
        return;
    }
    if(m_mainWindow.comboBox_name->currentText().isEmpty())
    {
        QMessageBox::warning(&m_mainWindow, tr("Ошибка логина"), Result().setError(Result::ReceiverEmpty).errorMessage());
        return;
    }
    if(!m_userlist.keys().contains(currentName()))
    {
        QMessageBox::warning(&m_mainWindow, tr("Ошибка логина"), Result().setError(Result::ReceiverIncorrect).errorMessage());
        return;
    }
    QByteArray receiverLogin = getReceiverLogin();
    if(m_device.myLogin().isEmpty())
    {
        QMessageBox::warning(&m_mainWindow, tr("Ошибка логина"), Result().setError(Result::myLoginError).errorMessage());
        return;
    }
    QFileInfo firstFileInfo(m_fileList.first()->inFileName());
    QString outFileName = QFileDialog::getSaveFileName(&m_mainWindow,
        tr("Укажите адрес контейнера для сохранения данных"),
        firstFileInfo.absoluteFilePath() + FileWorker::cryptedExtension(),
        tr("Зашифрованные файлы ", "параметр диалога выбора файла") + "(*" + FileWorker::cryptedExtension() + ")");
    if(outFileName.isEmpty())
        return;
    outFileName = QDir::toNativeSeparators(outFileName);
    QFile outFile(outFileName);
    if(!outFile.open(QFile::WriteOnly | QFile::Truncate))
    {
        QMessageBox::warning(&m_mainWindow, tr("Ошибка логина"), Result().setError(Result::CanNotOpenOutFile).errorMessage());
        return;
    }
    QDataStream out(&outFile);

    QByteArray userData(m_device.secretMessage() + m_receiverNamePrefix + receiverLogin + m_senderNamePrefix + m_device.myLogin());
    out << userData.toBase64();

    MyProgressDialog progress(tr("Шифрование файла"), tr("Отменить"), 0, 0, &m_mainWindow);
    progress.setMinimumDuration(0);

    m_device.setCryptLogin(receiverLogin);
    bool oneSuccess = false, allSuccess = true;
    for(QList<FileWorker *>::iterator iter = m_fileList.begin();
        iter != m_fileList.end() && !progress.wasCanceled(); iter++)
    {
        FileWorker *pWorker = *iter;
        if(outFileName == pWorker->inFileName())
        {
            pWorker->setErrorReason(tr("Файл является целевым архивом"));
            pWorker->setCompleted(FileWorker::HAS_ERRORS);
            continue;
        }
        if(!m_device.available() && !m_device.testConnection())
        {
            break;
        }
        qint64 size = out.device()->size();
        Result coreResult = prepareEncryptFileFolder(&progress, &out, pWorker);
        if(!coreResult)
        {
            if(!outFile.resize(size))
            {
                break;
            }
        }
        if(coreResult.m_error != Result::Skipped)
        {
            oneSuccess |= coreResult;
            allSuccess &= coreResult;
            pWorker->setErrorReason(coreResult.errorMessage());
            pWorker->setCompleted(coreResult ? FileWorker::SUCCESS : FileWorker::HAS_ERRORS);
        }

        if(coreResult)
            pWorker->setOutFileName(outFileName);
    }

    if(allSuccess && oneSuccess)
        m_mainWindow.showMessage(tr("Процесс зашифрования файлов завершен успешно"));
    else if(oneSuccess)
        m_mainWindow.showMessage(tr("Некоторые файлы не зашифрованы"));
    else
    {
        m_mainWindow.showMessage(tr("Зашифрование не выполнено"));
        outFile.remove();
    }
}

void Core::removeSelectedFilesFromTable()
{
    QList<QTableWidgetSelectionRange> ranges = m_mainWindow.tableWidget->selectedRanges();
    if(ranges.isEmpty())
        return;
    QSet<int> rowsSet;
    foreach (QTableWidgetSelectionRange range, ranges) {
        int topRow = range.topRow();
        int bottomRow = range.bottomRow();
        for(int row = bottomRow; row >= topRow;  row--)
        {
            rowsSet.insert(row);
        }
    }
    QList<int> rows = rowsSet.values();
    qSort(rows.begin(), rows.end(), qGreater<int>());
    foreach (int row, rows) {
        removeSelectedFilesFromTable(row);
    }
}

void Core::onTableSelectionChanged()
{
    QList<QTableWidgetSelectionRange> ranges = m_mainWindow.tableWidget->selectedRanges();
    if(ranges.isEmpty())
    {
        m_mainWindow.pushButton_removeFile->setEnabled(false);
        return;
    }
    QSet<int> rowsSet;
    foreach (QTableWidgetSelectionRange range, ranges) {
        int topRow = range.topRow();
        int bottomRow = range.bottomRow();
        for(int row = bottomRow; row >= topRow;  row--)
        {
            rowsSet.insert(row);
        }
    }
    QList<int> rows = rowsSet.values();
    m_mainWindow.pushButton_removeFile->setEnabled(rows.length());

}

void Core::removeSelectedFilesFromTable(int pos)
{
    if(pos < 0 || m_fileList.length() <= pos)
        return;
    m_mainWindow.tableWidget->removeRow(pos);
    FileWorker *pWorker = m_fileList.takeAt(pos);
    delete pWorker;

    updateControlsEnable();
}

void Core::removeSelectedFilesFromTable(QString fileName)
{
    for(int i = 0;  i < m_fileList.length();  i++)
        if(m_fileList.at(i)->inFileName() == fileName)
        {
            removeSelectedFilesFromTable(i);
            return;
        }
}

void Core::clearTable()
{
    for(int pos = m_fileList.length() - 1;  pos >= 0;  pos--)
    {
        m_mainWindow.tableWidget->removeRow(pos);
        FileWorker *pWorker = m_fileList.takeAt(pos);
        delete pWorker;
    }
    updateControlsEnable();
}

void Core::addDir()
{
    QString dirName = QFileDialog::getExistingDirectory(&m_mainWindow, tr("Выберите папку"));
    if(!dirName.isEmpty())
        addFile(dirName);
}

void Core::addFile()
{
    QStringList filePathes = QFileDialog::getOpenFileNames(&m_mainWindow, tr("Выберите файлы"));
    foreach (QString fileName, filePathes) {
        addFile(fileName);
    }
}

void Core::refreshSenderName(FileWorker *pWorker)
{
    if(pWorker->encrypt())
        return;
    QString sender;
    QByteArray shortSender = pWorker->senderLogin().left(pWorker->senderLogin().indexOf('\0'));
    if(pWorker->senderLogin() == m_device.myLogin())
        sender = "Me";
    else if(!m_userlist.values().contains(shortSender))
    {
        sender = "Unknown";
    }
    else
    {
        sender = m_userlist.key(shortSender);
    }

    pWorker->setSender(sender);
    translateTable(m_fileList.indexOf(pWorker));
}

QString Core::currentName() const
{
    return m_currentName;
}

void Core::setCurrentName(QString arg)
{
    if (m_currentName == arg)
        return;

    m_currentName = arg;
    for(int i = 0;  i < m_fileList.size();  i++) {
        if(m_fileList.at(i)->encrypt())
            m_fileList.at(i)->setCompleted(FileWorker::CREATED);
    }
    emit currentNameChanged(arg);
}

void Core::addFile(QString inFileName)
{
    if(inFileName.isEmpty() || inFileName.startsWith("--"))
        return;
    inFileName = QDir::toNativeSeparators(inFileName);
    for(QList<FileWorker *>::ConstIterator iter = m_fileList.constBegin();
        iter != m_fileList.constEnd();
        ++iter)
    {
        if((*iter)->inFileName() == inFileName)
        {
            (*iter)->getLabelStatus()->setPixmap(QPixmap(""));
            return;
        }
    }

    QByteArray receiverLogin, senderLogin;
    bool encrypt = FileWorker::encrypt(inFileName);
    if(!encrypt)
    {
        if(inFileName.isEmpty() || !QFile::exists(inFileName))
        {
            QMessageBox::warning(&m_mainWindow, tr("Ошибка файла"), tr("Нет такого входного файла!"));
            return;
        }
        QFile inFile(inFileName);
        if(!inFile.open(QFile::ReadOnly))
        {
            QMessageBox::warning(&m_mainWindow, tr("Ошибка файла"), tr("Нельзя открыть входной файл %1").arg(inFileName));
            return;
        }
        QDataStream in(&inFile);
        QByteArray userData;
        in >> userData;
        if(!m_device.startsWithMessage(userData))
        {
            QMessageBox::warning(&m_mainWindow, tr("Ошибка файла"), tr("Файл для расшифровки имеет неизвестный формат"));
            return;
        }
        userData = QByteArray::fromBase64(userData).remove(0, m_device.secretMessage().size());
        if(!userData.startsWith(m_receiverNamePrefix) || userData.indexOf(m_senderNamePrefix) != m_receiverNamePrefix.size() + AllThisThings::loginSize())
        {
            QMessageBox::warning(&m_mainWindow, tr("Ошибка файла"), tr("Файл для расшифровки имеет неизвестный формат"));
            return;
        }
        userData.remove(0, m_receiverNamePrefix.size());
        receiverLogin = QByteArray(userData.data(), AllThisThings::loginSize());
        userData.remove(0, AllThisThings::loginSize());
        userData.remove(0, m_senderNamePrefix.size());
        senderLogin = QByteArray(userData.data(), AllThisThings::loginSize());
        if(receiverLogin.isEmpty())
        {
            QMessageBox::warning(&m_mainWindow, tr("Ошибка логина"), tr("Указан неверный логин"));
            return;
        }
    }

    if(!m_mainWindow.pushButton_showHideFileArea->isChecked())
        m_mainWindow.pushButton_showHideFileArea->setChecked(true);


    QTableWidget *pTable = m_mainWindow.tableWidget;
    int rowNum = pTable->rowCount();

    FileWorker *pWorker = new FileWorker(rowNum, inFileName, this);
    pWorker->setSenderLogin(senderLogin);
    pWorker->setReceiverLogin(receiverLogin);


    m_fileList.append(pWorker);
    pTable->insertRow(rowNum);

    pTable->setItem(rowNum, 0, new QTableWidgetItem(pWorker->inFileName()));
    pTable->setItem(rowNum, 1, new QTableWidgetItem());
    pTable->setItem(rowNum, 2, new QTableWidgetItem());
    refreshSenderName(pWorker);

    pTable->item(rowNum, 0)->setToolTip(pWorker->inFileName());

    for(int i = pTable->columnCount() - 1;  i >= 0;  i--)
    {
        QTableWidgetItem *pItem = pTable->item(rowNum, i);
        pItem->setFlags(pItem->flags() ^ Qt::ItemIsEditable);
    }
    pTable->setCellWidget(rowNum, 2, pWorker->getLabelStatus());

    updateControlsEnable();
}

void Core::receiveMessageFromOtherInstance(QStringList list)
{
    foreach (QString str, list)
    {
            addFile(str);
    }
    m_mainWindow.forceShowOverAll();
}

void Core::onTranslate()
{
    recryptInputText();
    translateTable();
    updateControlsEnable();
}

void Core::showInGraphicalShell(const QString &pathIn)
{
#ifdef Q_OS_MAC
    QStringList args;
    args << "-e";
    args << "tell application \"Finder\"";
    args << "-e";
    args << "activate";
    args << "-e";
    args << "select POSIX file \""+pathIn+"\"";
    args << "-e";
    args << "end tell";
    QProcess::startDetached("osascript", args);
#endif
#ifdef Q_OS_WIN
    QFileInfo info(pathIn);
    QStringList args;
    if(info.isDir())
        args << "-p";
    args << "/select," << QDir::toNativeSeparators(pathIn);
    QProcess::startDetached("explorer", args);
#endif
}


Core::Result::operator bool() const
{
    return m_error == Ok;
}

Core::Result::Result()
{
    setError(Ok);
}

Core::Result& Core::Result::setError(Result::Error err)
{
    m_error = err;
    return *this;
}

Core::Result& Core::Result::setDeeperError(QString deeperErrorMessage)
{
    m_deeperErrorMessage = deeperErrorMessage;
    return setError(DeeperError);
}

QString Core::Result::errorMessage()
{
    switch(m_error)
    {
    case Ok:                        return QString();
    case DeeperError:               return m_deeperErrorMessage;
    case Skipped:                   return QString();
    case NoSuchInFile:              return tr("Файл не найден.");
    case CanNotOpenInFile:          return tr("Невозможно открыть исходный файл.");
    case CanNotCreatePath:          return tr("Не получилось создать папку.");
    case CanNotOpenOutFile:         return tr("Невозможно открыть результирующий файл.");
    case LoginNotFoundForFile:      return tr("Файл зашифрован для другого абонента.");
    case LoginNotFoundForText:      return tr("Сообщение предназначено для другого абонента.");
    case SenderUnknown:             return tr("Расшифрование сообщений от этого абонента невозможно.");
    case WrongCryptedDataFormat:    return tr("Данные для расшифровки имеют неизвестный формат.");
    case HashError:                 return tr("Ошибка целостности данных");
    case TextIsTooLarge:            return tr("Слишком большой текст! "
                                              "Сохраните его в файл и используйте шифрование файла для корректной работы!");
    case ReceiverEmpty:             return tr("Выберите абонента, для которого требуется зашифровать сообщение.");
    case ReceiverIncorrect:         return tr("Некорретный адресат. Выберите существующего адресата из списка.");
    case myLoginError:              return tr("Устройство не предоставило логин. Требуется переподключение.");
    }
    return QString();
}

