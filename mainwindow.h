#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QSystemTrayIcon>
#include <QSignalMapper>
#include "dialogselectuser.h"
#include "dialogabout.h"

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void forceShowOverAll();
    void loadDefaultLang();
private slots:
    void on_pushButton_showHideFileArea_toggled(bool);
    void focusToNext();
    void focusToPrev();
    void onDialogSelectUserAccepted();
    void clipboardDataChanged();
public slots:
    void setDeviceOpened(bool);
    void translate(QString);
    void updateNames(const QStringList &names);
signals:
    void fileAdded(QString);
    void registerShellExtension();
    void unregisterShellExtension();
    void changeLanguage(QString lang);
    void beforeClose();
protected:
    void closeEvent(QCloseEvent *event);
    void changeEvent(QEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void retranslateThis();

private:
    DialogSelectUser *m_pDialogSelectUser;
    DialogAbout *m_pDialogAbout;
    const int m_windowHeight, m_fileWidgetHeight;
    QString m_deviceSerial;
#ifndef QT_NO_SYSTEMTRAYICON
    void createTrayIcon();
    QSystemTrayIcon *m_pTrayIcon;
    QMenu *m_pTrayIconMenu;
    void createActions();
    QAction *m_pShowAbout;
    QAction *m_pRusLanguage;
    QAction *m_pEnLanguage;
    QSignalMapper *m_pSignalMapper;
#ifdef Q_OS_WIN
    QAction *m_pRegAction;
    QAction *m_pUnregAction;
#endif //Q_OS_WIN

    QAction *m_pMinimizeAction;
    QAction *m_pRestoreAction;
    QAction *m_pQuitAction;


public slots:
    void showMessage(QString message);

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void messageClicked();
#endif //QT_NO_SYSTEMTRAYICON
    void on_pushButton_copy_original_clicked();
    void on_plainTextEdit_original_textChanged();
    void on_pushButton_paste_original_clicked();
    void on_pushButton_clear_original_clicked();
    void on_pushButton_copy_transformed_clicked();
    void on_plainTextEdit_transformed_textChanged();
};

#endif // MAINWINDOW_H
