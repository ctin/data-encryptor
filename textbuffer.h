#ifndef TEXTBUFFER_H
#define TEXTBUFFER_H

#include <QIODevice>
#include <QBuffer>

class TextBuffer : public QIODevice
{
    Q_OBJECT
    Q_PROPERTY(bool encrypt READ encrypt WRITE setEncrypt NOTIFY encryptChanged)
public:
    explicit TextBuffer(const int part, QObject *parent = 0);
    qint64 writeData(const char *data, qint64 len);
    qint64 readData(char *data, qint64 maxlen);
    void setBuffer(const QString &text);
    bool reset();
    qint64 size() const;
    qint64 bytesAvailable() const;
    QBuffer m_buffer;

signals:
    void textReady(QString text);
    void cryptedTextReady(QString text);
public slots:
private:
    const int m_part;
    const int m_basedPart;

public:
    bool encrypt() const;
public slots:
    void setEncrypt(bool arg);
signals:
    void encryptChanged(bool arg);
private:
    bool m_encrypt;
};

#endif // TEXTBUFFER_H
