#include "fileworker.h"
#include <QFileInfo>
#include <QFileDialog>

FileWorker::FileWorker(int pos, QString inFileName, QObject *parent) :
    QObject(parent)
{
    m_pLabelStatus = new QLabel();
    m_pLabelStatus->setMargin(5);
    m_pLabelStatus->setAlignment(Qt::AlignCenter);

    setPos(pos);
    setProgress(0);
    setInFileName(inFileName);

    bool encrypt = !inFileName.endsWith(cryptedExtension());
    QString outFileName = inFileName;
    if(encrypt)
        outFileName += cryptedExtension();
    else
    {
        if(inFileName.endsWith(cryptedExtension()))
            outFileName.remove(QRegExp(cryptedExtension() + "$"));
    }
    connect(this, SIGNAL(completedChanged(Status)), this, SLOT(refreshCompleted()));
    connect(this, SIGNAL(errorReasonChanged(QString)), this, SLOT(refreshCompleted()));
    setOutFileName(outFileName);
    setCompleted(CREATED);
    //cellWidget()->label_process->setPixmap(QString(":/Images/") + (encrypt ? "encrypt" : "decrypt") + ".png");

}

void FileWorker::selectOutFile()
{
    bool encrypt = !inFileName().endsWith(cryptedExtension());
    QFileInfo fileInfo(outFileName());
    QString filter = encrypt ? cryptedExtension() : "";
    QString selectedFile = QFileDialog::getSaveFileName(qApp->activeWindow(), tr("Укажите файл"), fileInfo.absolutePath(), filter);
    if(selectedFile.isEmpty() || selectedFile == outFileName())
       return;
    setOutFileName(selectedFile);
}

QString FileWorker::cryptedExtension()
{
    return ".cpta";
}

bool FileWorker::encrypt(const QString &fileName)
{
    return !fileName.endsWith(cryptedExtension());
}

bool FileWorker::encrypt() const
{
    return encrypt(inFileName());
}

FileWorker::Status FileWorker::completed() const
{
    return m_completed;
}

QByteArray FileWorker::senderLogin() const
{
    return m_senderLogin;
}

QString FileWorker::errorReason() const
{
    return m_errorReason;
}

QByteArray FileWorker::receiverLogin() const
{
    return m_receiverLogin;
}

QLabel *FileWorker::getLabelStatus()
{
    return m_pLabelStatus;
}

QString FileWorker::inFileName() const
{
    return m_inFileName;
}

QString FileWorker::outFileName() const
{
    return m_outFileName;
}

int FileWorker::progress() const
{
    return m_progress;
}

QString FileWorker::lastError() const
{
    return m_lastError;
}

int FileWorker::pos() const
{
    return m_pos;
}

QString FileWorker::sender() const
{
    return m_sender;
}

void FileWorker::setInFileName(QString arg)
{
    if (m_inFileName != arg) {
        m_inFileName = arg;
        emit inFileNameChanged(arg);
    }
}

void FileWorker::setOutFileName(QString arg)
{
    if (m_outFileName != arg) {
        m_outFileName = arg;
        emit outFileNameChanged(arg);
    }
}

void FileWorker::setProgress(int arg)
{
    if (m_progress != arg) {
        m_progress = arg;
        emit progressChanged(arg);
    }
}

void FileWorker::setLastError(QString arg)
{
    if (m_lastError != arg) {
        m_lastError = arg;
        emit lastErrorChanged(arg);
    }
}

void FileWorker::setPos(int arg)
{
    if (m_pos != arg) {
        m_pos = arg;
        emit posChanged(arg);
    }
}

void FileWorker::setSender(QString arg)
{
    if (m_sender != arg) {
        m_sender = arg;
        emit senderChanged(arg);
    }
}

void FileWorker::refreshCompleted()
{
    QPixmap pixmap;
    QString tooltip;
    switch(completed()) {
    case CREATED:
        break;
    case SUCCESS:
        tooltip = tr("Операция успешно завершена");
        pixmap = QPixmap(":/Images/Ok-icon.png");
        break;
    case HAS_ERRORS:
        tooltip = errorReason();
        pixmap = QPixmap(":/Images/Error-icon.png");
        break;
    }
    getLabelStatus()->setToolTip(tooltip);
    getLabelStatus()->setPixmap(pixmap);
}

void FileWorker::setCompleted(FileWorker::Status arg)
{
    if (m_completed != arg) {
        m_completed = arg;

        emit completedChanged(arg);
    }
}

void FileWorker::setReceiverLogin(QByteArray arg)
{
    if (m_receiverLogin == arg)
        return;

    m_receiverLogin = arg;
    emit receiverLoginChanged(arg);
}

void FileWorker::setSenderLogin(QByteArray arg)
{
    if (m_senderLogin == arg)
        return;

    m_senderLogin = arg;
    emit senderLoginChanged(arg);
}

void FileWorker::setErrorReason(QString arg)
{
    if (m_errorReason == arg)
        return;

    m_errorReason = arg;
    emit errorReasonChanged(arg);
}
