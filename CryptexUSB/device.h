#ifndef CRYPTEXUSB_H
#define CRYPTEXUSB_H

#include <QtGlobal>


#ifdef Q_OS_WIN
#include <windows.h>
#endif //Q_OS_WIN
#ifdef SERIAL
#include <QtSerialPort>
#endif //SERIAL
#ifdef LIBUSB
#include "libusb.h"
#endif //LIBUSB
#include <QString>
#include <QStringList>

class Device
{
public:
    explicit Device();
    virtual ~Device();
    bool open(const QString &systemPath);
    bool close();
    int read(char *buffer, int maxSize);
    int write(char *buffer, const int size);
    static QStringList getAvailablePorts(quint32 *errorCode = 0);

    enum {
        INTERNAL_BUFFER_SIZE = 1200
    };

private:
#ifdef SERIAL
    QSerialPort m_port;
#endif //SERIAL
#if defined(Q_OS_WIN)
    HANDLE m_readHandle, m_writeHandle;

    static std::string utf8_encode(const std::wstring &wstr);
    static std::wstring utf8_decode(const std::string &str);
#endif //Q_OS_WIN
#ifdef LIBUSB
    bool checkSudo() const;
    libusb_device_handle *device_handle_;
    libusb_context *ctx_;
    QByteArray m_password;
#endif //LIBUSB


};

#endif // CRYPTEXUSB_H
