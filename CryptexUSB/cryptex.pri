INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD


win32 {
    !CONFIG(SERIAL) {
        SOURCES += $$PWD/device_windows.cpp
        LIBS             += -lsetupapi
    }
    CONFIG(SERIAL) {
        DEFINES += SERIAL
        QT += serialport
        SOURCES += $$PWD/device_serial.cpp
    }
}
else : mac {
    CONFIG(BOOST_ASIO) {
        DEFINES += BOOST_ASIO
        INCLUDEPATH += /opt/local/include/boost/
        LIBS += /opt/local/lib/libboost_system-mt.dylib
        SOURCES += $$PWD/asio_device.cpp
    } else : CONFIG(LIBUSB) {
        DEFINES += LIBUSB
        INCLUDEPATH += $$PWD/libusb
        LIBS += $$PWD/libusb/libusb-1.0.dylib
        SOURCES += $$PWD/usb_device.cpp
    } else {
        DEFINES += SERIAL
        QT += serialport
        SOURCES += $$PWD/device_serial.cpp
    }
}
else {
        DEFINES += SERIAL
        QT += serialport
        SOURCES += $$PWD/device_serial.cpp
}

SOURCES += $$PWD/cryptexlingvo.cpp \
    $$PWD/cryptexdevice.cpp \
    $$PWD/allthisthings.cpp

HEADERS  += $$PWD/cryptexlingvo.h \
    $$PWD/cryptexdevice.h \
    $$PWD/device.h \
    $$PWD/allthisthings.h


