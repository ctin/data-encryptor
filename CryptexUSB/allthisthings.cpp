#include "allthisthings.h"

struct InWorkManager
{
    InWorkManager(AllThisThings *pThisThings) : m_pThisThings(pThisThings) {
        Q_ASSERT(m_pThisThings);
        wasInwork = m_pThisThings->inWork();
        if(!wasInwork)
            m_pThisThings->setInWork(true);
    }
    ~InWorkManager() {
        if(!wasInwork)
            m_pThisThings->setInWork(false);
    }
    bool wasInwork;
    AllThisThings *m_pThisThings;
};

AllThisThings::AllThisThings(QString systemPathArg, QObject *parent)
    : CryptexDevice(systemPathArg, parent), m_hash(QCryptographicHash::Sha1),
    m_secretMessage("$%^123Super Crypted Text: ")
{
    setInWork(false);
}

AllThisThings::~AllThisThings()
{

}

bool AllThisThings::refreshSerialAndMyLogin()
{
    InWorkManager manager(this);
    QByteArray arr;
    bool result = transaction(Lingvo::CMD_REQ_SERVER_ADDRESS, &arr);
    if(result)
    {
        arr.remove(0, 256 + 4 + 4 + 2); //пиздец. Не смотрите сюда.
        QByteArray data = arr.left(16);
        if(data.size() < 4)
            return false;
        QString serial;
        for(int i = 3;  i >= 0;  i--)
        {
            serial.append(QString::number(static_cast<quint8>((data.at(i) & 0xf0) >> 4), 16));
            serial.append(QString::number(static_cast<quint8>(data.at(i) & 0x0f), 16));
        }
        setSerial(serial);
        arr.remove(0, 16);
        if(arr.size() <= AllThisThings::loginSize())
        {
            return false;
        }
        setMyLogin(arr.left(AllThisThings::loginSize()));
    }
    return result;
}

bool AllThisThings::testConnection()
{
    InWorkManager manager(this);
    return transaction(Lingvo::CMD_ECHO);
}

CryptexDevice::Result AllThisThings::initCipherMode(qint32 size)
{
    InWorkManager manager(this);
    const qint32 id = 0; //либо строкой логин, либо id
    const int paramSize = 4;
    QByteArray commandToInit;
    commandToInit.append(reinterpret_cast<const char*>(&id), paramSize);
    commandToInit.append(reinterpret_cast<const char*>(&size), paramSize);
    commandToInit.append(cryptLogin());
    return transaction(Lingvo::CMD_SET_CIPHER_MODE, Lingvo::KEY_EMAIL, &commandToInit);
}

bool AllThisThings::processDialog(QProgressDialog *pProgress, qint64 readSize, AllThisThings::Result *pResult)
{

    Q_ASSERT(pResult);
    if(!pResult)
        return pResult;
    if(pProgress)
    {
        if(pProgress->isVisible())
            qApp->processEvents();
        if(pProgress->wasCanceled())
        {
            pResult->setError(Result::Cancelled);
        }
        else
        {
            qint64 readBlock = qCeil((1.0 * readSize) / MAX_BLOCK_SIZE);
            pProgress->setValue(readBlock);
        }
        if(pProgress->isVisible())
            qApp->processEvents();
    }
    return pResult;
}

int AllThisThings::loginSize()
{
    return 24;
}

int AllThisThings::nameSize()
{
    return 32;
}

qint64 AllThisThings::encryptedSize(qint64 originalSize) const
{
    qint64 partsCount = qCeil((1.0 * originalSize) / MAX_PART_SIZE);
    return originalSize + partsCount * CRYPTOINFO_SIZE + m_hash.result().size();
}

bool AllThisThings::searchDevice()
{
    InWorkManager manager(this);
    QString defaultPath = systemPath();
    QStringList ports = Device::getAvailablePorts();
    if(defaultPath.isEmpty() && ports.isEmpty())
    {
        return false;
    }
    close();
    setWaitTime(300);
    bool result = false;
    if(!defaultPath.isEmpty())
        ports.prepend(defaultPath);
    foreach (QString str, ports) {
        setSystemPath(str);
        result = refreshSerialAndMyLogin();
        if(result)
            break;
    }
    resetWaitTime();
    return result;
}

QMap<QString, QByteArray> AllThisThings::getBindingsTable()
{
    InWorkManager manager(this);
    QByteArray arr;
    bool result = open();
    for (int index = 0; index < 0x7FFF && result; index++) //7FFF- максимальный размер таблицы
    {
        quint16 param = index;
        QByteArray tmpArr;
        result = transaction(Lingvo::CMD_REQ_BINDINGS_TABLE, &param, &tmpArr);
        if( result)
        {
            if (param == 0x8001) //error
            {
                result = false;
            }
            arr.append(tmpArr);
            if (param == 0x8000) // end
                break;
        }
        else
            break;
    }
    close();

    QMap<QString, QByteArray> userList;
    if(!result)
        return userList;
    const char *data = arr.data();
    int pos = 0;
    const int size  = arr.size();
    while(pos < size)
    {
        int len = data[pos++];
        QByteArray login(QByteArray(data + pos, len));
        pos += len;
        len = data[pos++];
        QString name(QByteArray(data + pos, len));
        pos += len;
        userList.insert(name, login);
    }
    return userList;
}

struct Opener
{
    Opener(CryptexDevice *pDevice) : m_pDevice(pDevice) {m_opened = m_pDevice->open();}
    ~Opener() {m_pDevice->close();}
    CryptexDevice *m_pDevice;
    bool m_opened;
};

AllThisThings::Result AllThisThings::cryptData(QDataStream *pInStream, QDataStream *pOutStream, bool encrypt, QProgressDialog *pProgress, qint64 totalLength)
{
    InWorkManager manager(this);
    Q_ASSERT(pInStream);
    Q_ASSERT(pOutStream);

    m_hash.reset();
    qint64 readSize = 0;
    char buffer[INTERNAL_BUFFER_SIZE];
    if(totalLength == -1) //size of data to crypt
        totalLength = pInStream->device()->size();
    if(!encrypt)
        totalLength -= m_hash.result().size();
    if(pProgress)
    {
        pProgress->setMinimum(0);
        pProgress->setMaximum(qCeil((1.0 * totalLength) / MAX_BLOCK_SIZE));
        pProgress->setValue(0);
    }
    Result result;
    if(!encrypt)
    {
        bool needDisplayHashInfo = totalLength > 10 * 1000 * 1000;
        QString labelText;
        if(pProgress && needDisplayHashInfo)
        {
            labelText = pProgress->labelText();
            pProgress->setLabelText(labelText + "\n" + tr("Проверка целостности сообщения"));
        }
        const qint64 pos = pInStream->device()->pos();
        while(result && readSize < totalLength)
        {
            const qint64 bytesRemains = totalLength - readSize;
            qint64 partSize = static_cast<qint64>(MAX_PART_SIZE) + CRYPTOINFO_SIZE; //сколько обработаем за раз
            partSize = qMin(partSize, bytesRemains); //если фактически данных меньше то берем сколько есть
            qint64 operationsCount = qCeil((1.0 * partSize) / MAX_BLOCK_SIZE);
            for(int block = 0;  block < operationsCount;)
            {
                qint64 buffSize = qMin(static_cast<qint64>(DEVICE_PACKET_BUFF), operationsCount - block);
                for(int op = 0;  op < buffSize;  op++, block++)
                {
                    qint64 currentPartSize = qMin(static_cast<qint64>(MAX_BLOCK_SIZE), partSize);
                    Q_ASSERT(currentPartSize);
                    qint64 readBytes = pInStream->readRawData(buffer, currentPartSize);
                    if(readBytes != currentPartSize)
                    {
                        return result.setError(Result::ReadStreamError);
                    }
                    readSize += readBytes;
                    partSize -= readBytes;
                    m_hash.addData(buffer, readBytes);
                    if(!processDialog(pProgress, readSize, &result))
                        return result;
                }
            }
        }
        if(result)
        {
            qint64 readBytes = pInStream->readRawData(buffer, m_hash.result().size());
            if(readBytes == -1)
            {
                return result.setError(Result::ReadStreamError);
            }
            else
            {
                QByteArray readHash = QByteArray(buffer, readBytes);
                if(m_hash.result() != readHash)
                    return result.setError(Result::HashError);
            }
        }
        if(pProgress && needDisplayHashInfo)
            pProgress->setLabelText(labelText);
        if(!pInStream->device()->seek(pos))
        {
            return result.setError(Result::ReadStreamError);
        }
        readSize = 0;
    }
    while(result && readSize < totalLength)
    {
        const qint64 bytesRemains = totalLength - readSize;
        qint64 partSize = static_cast<int>(MAX_PART_SIZE);
        if(!encrypt)
            partSize += CRYPTOINFO_SIZE;
        partSize = qMin(partSize, bytesRemains);
        Q_ASSERT(partSize);
        Opener opener(this);
        if(!opener.m_opened)
        {
            result.setError(Result::OpenError);
            setAvailable(false);
            break;
        }
        CryptexDevice::Result res = initCipherMode(partSize);
        if(!res)
        {
            if(res.m_error == CryptexDevice::Result::LingvoError) //передача своего логина для операции шифрования вызовет неопределенное поведение  программы. Может не инициализировать логин отправителя, но инициализиорвать собственный и зашифрует для себя. Лол.
            {
                result.setError(Result::InitError);
                break;
            }
            else
            {
                result.setError(Result::IOError);
                break;
            }
        }

        qint64 operationsCount = qCeil((1.0 * partSize) / MAX_BLOCK_SIZE);
        unsigned char command = encrypt ? Lingvo::CMD_ENCRYPT_DATA : Lingvo::CMD_DECRYPT_DATA;
        for(qint64 block = 0;  block < operationsCount && result;)
        {
            qint64 buffSize = qMin(static_cast<qint64>(DEVICE_PACKET_BUFF), operationsCount - block);
            for(int op = 0;  op < buffSize && result;  op++)
            {
                qint64 currentPartSize = qMin(static_cast<qint64>(MAX_BLOCK_SIZE), partSize);
                qint64 readBytes = pInStream->readRawData(buffer, currentPartSize);

                if(readBytes != currentPartSize)
                {
                    result.setError(Result::ReadStreamError);
                    break;
                }

                readSize += readBytes;
                partSize -= readBytes;
                quint16 param = block + op + 1;
                if(!write(Lingvo::createPacket(command, param, QByteArray(buffer, readBytes))))
                {
                    result.setError(Result::IOError);
                    setAvailable(false);
                    break;
                }
                setAvailable(true);
            }
            int op = 0;
            QByteArray readData;
            while(op < buffSize && result)
            {
                bool checked = Lingvo::adjustAndCheck(&readData, command);
                static QTime time;
                time.restart();
                quint32 elaplsed = 0;
                while(result && !checked && elaplsed < waitTime())
                {
                    if(!read(&readData))
                    {
                        result.setError(Result::IOError);
                        setAvailable(false);
                        break;
                    }
                    elaplsed = time.elapsed();
                    checked = Lingvo::adjustAndCheck(&readData, command);
                }
                if(!available())
                {
                    result.setError(Result::IOError);
                    break;
                }
                checked &= Lingvo::getCommand(readData) != Lingvo::CMD_ERROR;
                checked &= Lingvo::getCommand(readData) == command;
                ushort param = Lingvo::getParam(readData);
                checked &=  param == (++block);
                if(!checked)
                {
                    result.setError(Result::CryptingError);
                    setAvailable(false);
                    break;
                }
                op++;
                setAvailable(true);
                QByteArray data = Lingvo::getData(readData);
                Lingvo::removeFirstPacket(&readData);
                if(encrypt)
                    m_hash.addData(data);
                qint64 written = pOutStream->writeRawData(data.data(), data.size());
                if(written != data.size())
                {
                    result.setError(Result::WriteStreamError);
                    break;
                }
            }
            processDialog(pProgress, readSize, &result);
        }
    }
    if(result)
    {
        if(encrypt)
        {

            qint64 written = pOutStream->writeRawData(m_hash.result().data(), m_hash.result().length());
            if(written == -1)
            {
                result.setError(Result::WriteStreamError);
            }

        }
        else
        {
            qint64 readBytes = pInStream->readRawData(buffer, m_hash.result().size()); //read hash from and again , need to close transaction
            if(readBytes == -1)
            {
                return result.setError(Result::ReadStreamError);
            }
        }
    }
    return result;
}

const QByteArray& AllThisThings::secretMessage() const
{
    return m_secretMessage;
}

bool AllThisThings::startsWithMessage(const QByteArray &data) const
{
    static const QByteArray basedMessage = m_secretMessage.toBase64().left(m_secretMessage.length());
    return data.startsWith(basedMessage);
}

bool AllThisThings::adjustToSecretMessage(QByteArray *pData) const
{
    Q_ASSERT(pData);
    static const QByteArray basedMessage = m_secretMessage.toBase64().left(m_secretMessage.length());

    int index = pData->indexOf(basedMessage);
    if(index == -1)
        return false;
    pData->remove(0, index);
    return true;
}

QString AllThisThings::serial() const
{
    return m_serial;
}

QByteArray AllThisThings::myLogin() const
{
    return m_myLogin;
}

QByteArray AllThisThings::cryptLogin() const
{
    return m_cryptLogin;
}

bool AllThisThings::inWork() const
{
    return m_inWork;
}

void AllThisThings::setSerial(QString arg)
{
    if (m_serial != arg) {
        m_serial = arg;
        serialChanged(arg);
    }
}

void AllThisThings::setMyLogin(QByteArray arg)
{
    if (m_myLogin != arg) {
        m_myLogin = arg;
        emit myLoginChanged(arg);
    }
}

void AllThisThings::setCryptLogin(QByteArray arg)
{
    if (m_cryptLogin == arg)
        return;

    m_cryptLogin = arg;
    emit cryptLoginChanged(arg);
}

void AllThisThings::setInWork(bool arg)
{
    if (m_inWork == arg)
        return;

    m_inWork = arg;
    emit inWorkChanged(arg);
}


AllThisThings::Result::operator bool() const
{
    return m_error == Ok;
}

AllThisThings::Result::Result()
{
    setError(Ok);
}

AllThisThings::Result& AllThisThings::Result::setError(AllThisThings::Result::Error err)
{
    m_error = err;
    return *this;
}

QString AllThisThings::Result::errorMessage()
{
    switch(m_error)
    {
    case Ok:                return QString();
    case Cancelled:         return tr("Операция отменена.");
    case OpenError:         return tr("Не получилось подключиться к шифратору.");
    case InitError:         return tr("Ошибка перехода шифратора в режим шифрования.");
    case IOError:           return tr("Потеряна связь с шифратором. Операция выполнена не полностью.");
    case CryptingError:     return tr("Ошибка в процессе шифрования.");
    case ReadStreamError:   return tr("Ошибка считывания данных для шифрования.");
    case WriteStreamError:  return tr("Ошибка записи обработанных данных.");
    case HashError:         return tr("Ошибка целостности данных.");
    }
    return QString();
}
