#ifndef CRYPTEXDEVICE_H
#define CRYPTEXDEVICE_H

#include "cryptexlingvo.h"
#include <QtCore>
#include "device.h"


class CryptexDevice : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString systemPath READ systemPath WRITE setSystemPath NOTIFY systemPathChanged RESET resetSystemPath)
    Q_PROPERTY(QByteArray readBuffer READ readBuffer WRITE setReadBuffer NOTIFY readBufferChanged RESET resetReadBuffer)
    Q_PROPERTY(QByteArray writeBuffer READ writeBuffer NOTIFY writeBufferChanged RESET writeBuffer)
    Q_PROPERTY(bool isOpen READ isOpen NOTIFY isOpenChanged RESET resetIsOpen)
    Q_PROPERTY(bool available READ available WRITE setAvailable NOTIFY availableChanged)
    Q_PROPERTY(quint8 packetNum READ packetNum WRITE setPacketNum NOTIFY packetNumChanged RESET resetPacketNum)
    Q_PROPERTY(quint32 waitTime READ waitTime WRITE setWaitTime NOTIFY waitTimeChanged RESET resetWaitTime)

public:
    struct Result {
        Result();
        enum Error {
            Ok,
            OpenError,
            CloseError,
            ReadError,
            WriteError,
            TimeoutError,
            LingvoError
        };
        Error m_error;
        operator bool() const;
        Result& setError(Error err);
        QString errorMessage();
    };
    explicit CryptexDevice(QString systemPath = QString(), QObject *parent = 0);
    ~CryptexDevice();
#ifdef Q_OS_MAC
    enum {  DEVICE_PACKET_BUFF	=	1 }; //размер буфера внутри прибора в пакетах
#else
    enum {  DEVICE_PACKET_BUFF	=	3 }; //размер буфера внутри прибора в пакетах
#endif //Q_OS_MAC
#if defined(Q_OS_WIN) && !defined(SERIAL)
    enum {  IDEAL_WAIT_TIME    =   3000 }; //TODO: поменять когда коробка не будет ожидать набора пина
#else
    enum {  IDEAL_WAIT_TIME    =   4000 }; //TODO: поменять когда коробка не будет ожидать набора пина
#endif //SERIAL
    enum {  INTERNAL_BUFFER_SIZE = 1200 };
    enum {  MAX_BLOCK_SIZE = 941 };
    enum {  MAX_PART_SIZE = 0x1000 * MAX_BLOCK_SIZE }; //размер шифруемых за сессию данных
    enum {  CRYPTOINFO_SIZE = 36 };

    Result transaction(const quint8 &command, QByteArray *data);
    Result transaction(const quint8 &command, quint16 *param = 0, QByteArray *data = 0);
    Result transaction(const quint8 &command, const quint16 &param, QByteArray *data = 0);

    bool open(QString path);
    bool open();
    bool close();
    bool read(QByteArray *arr);
    bool write(QByteArray arr);
private:
    Result _transaction(const quint8 &command, quint16 *param, QByteArray *data);

public:
    Device m_device;

    QString systemPath() const;
    QByteArray readBuffer() const;
    QByteArray writeBuffer() const;
    bool isOpen() const;
    quint8 packetNum() const;
    quint32 waitTime() const;
    bool available() const;

public slots:
    void setSystemPath(QString arg);
    void setReadBuffer(QByteArray arg);
    void setWriteBuffer(QByteArray arg);
    void setIsOpen(bool arg);
    void setPacketNum(quint8 arg);
    void setWaitTime(quint32 arg);

    void resetSystemPath();
    void resetReadBuffer();
    void resetWriteBuffer();
    void resetIsOpen();
    void resetPacketNum();
    void resetWaitTime();
    void setAvailable(bool arg);

signals:
    void systemPathChanged(QString arg);
    void readBufferChanged(QByteArray arg);
    void writeBufferChanged(QByteArray arg);
    void isOpenChanged(bool arg);
    void packetNumChanged(quint8 arg);
    void waitTimeChanged(quint32 arg);
    void availableChanged(bool arg);

private:
    QString m_systemPath;
    QByteArray m_readBuffer;
    QByteArray m_writeBuffer;
    bool m_isOpen;
    char m_packetNum;
    quint32 m_waitTime;
    bool m_available;
};

#endif // CRYPTEXDEVICE_H
