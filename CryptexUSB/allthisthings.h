#ifndef ALLTHISTHINGS_H
#define ALLTHISTHINGS_H

#include "cryptexdevice.h"
#include <QProgressDialog>
#include <QCryptographicHash>

class AllThisThings : public CryptexDevice
{
    Q_OBJECT
    Q_PROPERTY(QString serial READ serial WRITE setSerial NOTIFY serialChanged)
    Q_PROPERTY(QByteArray myLogin READ myLogin WRITE setMyLogin NOTIFY myLoginChanged)
    Q_PROPERTY(QByteArray cryptLogin READ cryptLogin WRITE setCryptLogin NOTIFY cryptLoginChanged)
    Q_PROPERTY(bool inWork READ inWork WRITE setInWork NOTIFY inWorkChanged)
public:
    struct Result {
        Result();
        enum Error {
            Ok,
            Cancelled,
            OpenError,
            InitError,
            IOError,
            CryptingError,
            ReadStreamError,
            WriteStreamError,
            HashError
        };
        Error m_error;
        operator bool() const;
        Result& setError(Error err);
        QString errorMessage();
    };

    AllThisThings(QString systemPath = QString(), QObject *parent = 0);
    ~AllThisThings();
    static int loginSize();
    static int nameSize();
    qint64 encryptedSize(qint64 originalSize) const;
    bool searchDevice();
    bool refreshSerialAndMyLogin();
    QMap<QString, QByteArray> getBindingsTable();
    Result cryptData(QDataStream *pIn, QDataStream *pOut, bool encrypt, QProgressDialog *pProgress = 0, qint64 length = -1);
    const QByteArray& secretMessage() const;
    bool startsWithMessage(const QByteArray &data) const;
    bool adjustToSecretMessage(QByteArray *pData) const;
private:
    CryptexDevice::Result initCipherMode(qint32 size);
    QCryptographicHash m_hash;
    const QByteArray m_secretMessage;
    bool processDialog(QProgressDialog *pProgress, qint64 readSize, Result *pResult);

public slots:
    bool testConnection();

public:
    QString serial() const;
    QByteArray myLogin() const;
    QByteArray cryptLogin() const;
    bool inWork() const;

signals:
    void serialChanged(QString);
    void myLoginChanged(QByteArray arg);
    void cryptLoginChanged(QByteArray arg);
    void inWorkChanged(bool arg);

public slots:
    void setSerial(QString arg);
    void setMyLogin(QByteArray arg);
    void setCryptLogin(QByteArray arg);
    void setInWork(bool arg);

private:
    QString m_serial;
    QByteArray m_myLogin;
    QByteArray m_cryptLogin;
    bool m_inWork;
};

#endif // ALLTHISTHINGS_H
