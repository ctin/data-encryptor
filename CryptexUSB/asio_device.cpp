#include <QDebug>
#include <QDir>

#include "asio_device.h"

AsioDevice::AsioDevice() : m_port_(m_io_) {}

AsioDevice::~AsioDevice() { close(); }

bool AsioDevice::open(const QString &systemPath) {

  m_port_.open(systemPath.toStdString());
  if (m_port_.is_open()) {
    m_port_.set_option(boost::asio::serial_port_base::baud_rate(9600));
    m_port_.set_option(boost::asio::serial_port_base::character_size(8));
    m_port_.set_option(boost::asio::serial_port_base::flow_control(
        boost::asio::serial_port_base::flow_control::none));
    m_port_.set_option(boost::asio::serial_port_base::parity(
        boost::asio::serial_port_base::parity::none));
    m_port_.set_option(boost::asio::serial_port_base::stop_bits(
        boost::asio::serial_port_base::stop_bits::one));
  }
  return m_port_.is_open();
}
bool AsioDevice::close() {
  if (m_port_.is_open()) {
    m_port_.close();
  }
  return true;
}
int AsioDevice::read(char *buffer, int maxSize) {
  return m_port_.read_some(boost::asio::buffer((unsigned char *)buffer,maxSize));
}
int AsioDevice::write(char *buffer, const int size) {
  return m_port_.write_some(boost::asio::buffer((unsigned char *)buffer,size));
}

QStringList AsioDevice::getAvailablePorts(unsigned long *errorCode) {
    Q_UNUSED(errorCode);
    QDir lsDev("/dev/");
    lsDev.setNameFilters(QStringList() << "cu.u*");
    QFileInfoList list = lsDev.entryInfoList(QDir::System);
    QStringList ports;
    foreach (QFileInfo info, list) {
        ports << info.absoluteFilePath();
    }
    return ports;
}
