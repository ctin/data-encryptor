#include "device.h"
#include "windows.h"
#include <SetupAPI.h>
#include <initguid.h>
#include <sstream>
#include <locale>
#include <QThread>

DEFINE_GUID(GUID_CLASS_ANCORT,
    0xf7947e18, 0x7ab6, 0x45d3, 0x98, 0x14, 0xf3, 0xd0, 0xdf, 0xdd, 0xe, 0x1c);

Device::Device()
{
    m_readHandle = INVALID_HANDLE_VALUE;
    m_writeHandle = INVALID_HANDLE_VALUE;
}

Device::~Device()
{
}

bool Device::open(const QString &systemPath)
{
    m_readHandle = INVALID_HANDLE_VALUE;
    m_writeHandle = INVALID_HANDLE_VALUE;

    m_writeHandle = CreateFile(
         utf8_decode(systemPath.toStdString() + "\\PIPE00").c_str(),
         GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_OVERLAPPED,
        NULL);

    if(m_writeHandle == INVALID_HANDLE_VALUE)
    {
        return false;
    }

    m_readHandle = CreateFile(
        utf8_decode(systemPath.toStdString() + "\\PIPE01").c_str(),
        GENERIC_READ ,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_OVERLAPPED,
        NULL);

    if(m_readHandle == INVALID_HANDLE_VALUE)
    {
        return false;
    }

    return true;
}

bool Device::close()
{
    bool result = true;
    if (m_readHandle != INVALID_HANDLE_VALUE)
    {
        if(!CloseHandle(m_readHandle))
        {
            result = false;
        }
        m_readHandle = INVALID_HANDLE_VALUE;
    }
    if (m_writeHandle != INVALID_HANDLE_VALUE)
    {
        if(!CloseHandle(m_writeHandle))
        {
            result = false;
        }
        m_writeHandle = INVALID_HANDLE_VALUE;
    }
    return result;
}

int Device::read(char *buffer, int maxSize)
{
    OVERLAPPED overlapped;
    overlapped.Offset = 0;
    overlapped.OffsetHigh = 0;
    overlapped.hEvent = 0;

    DWORD readSize = 0;
    ReadFile(
        m_readHandle,
        buffer,
        maxSize,
        &readSize,
        &overlapped);
    if(!GetOverlappedResult(m_readHandle,
        &overlapped,
        &readSize,
        TRUE))
    {
        return (GetLastError() == ERROR_IO_INCOMPLETE) ? 0 : -1;
    }
    return readSize;
}
int Device::write(char *buffer, const int maxSize)
{
    OVERLAPPED overlapped;
    overlapped.Offset = 0;
    overlapped.OffsetHigh = 0;
    overlapped.hEvent = 0;

    DWORD lpNumberOfBytesWritten;

    WriteFile(m_writeHandle,
            buffer,
            maxSize,
            &lpNumberOfBytesWritten,
            &overlapped);
    if(!GetOverlappedResult(m_writeHandle,
        &overlapped,
        &lpNumberOfBytesWritten,
        TRUE))
        return -1;
    return lpNumberOfBytesWritten;
}

class DeviceInterfaceDetailDataWrapper
{
public:
    DeviceInterfaceDetailDataWrapper(PSP_DEVICE_INTERFACE_DETAIL_DATA *data, const ULONG predictedLength) : m_data(data)
    {
        *m_data = static_cast<SP_DEVICE_INTERFACE_DETAIL_DATA*>(malloc(predictedLength));
        if (*m_data) {
            (*m_data)->cbSize = sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);
        }
    }
    ~DeviceInterfaceDetailDataWrapper() {
        free(*m_data);
    }
private:
    PSP_DEVICE_INTERFACE_DETAIL_DATA *m_data;


};


QStringList Device::getAvailablePorts(quint32 *errorCode)
{
    HDEVINFO                            hardwareDeviceInfo;
    SP_DEVICE_INTERFACE_DATA            deviceInterfaceData;
    ULONG                               predictedLength = 0;
    ULONG                               requiredLength = 0;
	if (errorCode)
		*errorCode = 0;

    QStringList currentUSBdevices;

    // ---- Open DeviceInfo handle
    hardwareDeviceInfo = SetupDiGetClassDevs(
        &GUID_CLASS_ANCORT,
        NULL, // Define no enumerator (global)
        NULL, // Define no
        (DIGCF_PRESENT | // Only Devices present
        DIGCF_DEVICEINTERFACE));

    if (hardwareDeviceInfo == INVALID_HANDLE_VALUE)
    {
		if (errorCode)
			*errorCode = GetLastError();
        return currentUSBdevices;
    }


    // ---- Enumerate devices

    deviceInterfaceData.cbSize = sizeof (SP_DEVICE_INTERFACE_DATA);

    for (int no = 0;  ;  no++)
    if (SetupDiEnumDeviceInterfaces(hardwareDeviceInfo,
        0, // No care about specific PDOs
        &GUID_CLASS_ANCORT,
        no, // sequencial nomer
        &deviceInterfaceData)) {

        PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL ;

        //
        // Allocate a function class device data structure to
        // receive the information about this particular device.
        //
        //
        // First find out required length of the buffer
        //

        if(!SetupDiGetDeviceInterfaceDetail(
            hardwareDeviceInfo,
            &deviceInterfaceData,
            NULL, // probing so no output buffer yet
            0, // probing so output buffer length of zero
            &requiredLength,
            NULL))
        {
            if(GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            {
				if (errorCode)
					*errorCode = GetLastError();
                break;
            }
        }

        predictedLength = requiredLength;

        DeviceInterfaceDetailDataWrapper wrap(&deviceInterfaceDetailData, predictedLength);
        if(!deviceInterfaceDetailData)
        {
			if (errorCode)
				*errorCode = GetLastError();
            break;
        }

        if (!SetupDiGetDeviceInterfaceDetail(
            hardwareDeviceInfo,
            &deviceInterfaceData,
            deviceInterfaceDetailData,
            predictedLength,
            &requiredLength,
            NULL))
        {
			if (errorCode)
				*errorCode = GetLastError();
            break;
        }
        std::wstring wstr(deviceInterfaceDetailData->DevicePath);
        currentUSBdevices.push_back(QString::fromStdString(utf8_encode(wstr)));
    }
    else if (ERROR_NO_MORE_ITEMS == GetLastError()) {
        break;
    }
    else {
		if (errorCode)
			*errorCode = GetLastError();
        break;
    }
    return currentUSBdevices;
}

std::string Device::utf8_encode(const std::wstring &wstr)
{
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo( size_needed, 0 );
    WideCharToMultiByte                  (CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
}

// Convert an UTF8 string to a wide Unicode String
std::wstring Device::utf8_decode(const std::string &str)
{
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo( size_needed, 0 );
    MultiByteToWideChar                  (CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}
