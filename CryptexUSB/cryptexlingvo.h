#ifndef CRYPTEXLINGVO_H
#define CRYPTEXLINGVO_H

#include <QByteArray>

namespace Lingvo
{

    QByteArray createPacket(quint8 command, quint16 param = 0, QByteArray data = QByteArray(), quint16 packetNum = -1); //не статическая для многопоточности
    bool checkPacket(const QByteArray &buff, const quint16 packetNum = 0xffff);
    bool checkPacket(const char *data, const int len, const quint16 packetNum = 0xffff);

    quint8 getCommand(const QByteArray &validPacket);
    quint8 getCommand(const char *data, const int len);
    bool adjustAndCheck(QByteArray *packet, const quint8 command = 0xff);
    bool removeFirstPacket(QByteArray *packet);
    quint16 getParam(const QByteArray &validPacket);
    quint16 getDataLen(const QByteArray &validPacket);
    quint16 getDataLen(const char *data, const int len);
    quint8 getPacketNum(const QByteArray &validPacket);
    QByteArray getData(const QByteArray &validPacket);

    enum LINGVO_CONSTS{
        HEADER_SIZE         =   0x08,
        LINGVO_MARKER       =   0x55,
    };
    enum {
        MARKER_POS      =   0,
        COMMAND_POS     =   1,
        PARAM_POS       =   2,
        DATALEN_POS     =   4,
        PACKETNUM_POS   =   6,
        CRC8_POS        =   7
    };
    enum
    {
		CMD_ECHO							=	0x01,
		CMD_CRYPTOCONT_CREATE_HEADER		=	0x32,    /*!<Команда  - создание заголовка криптоконтейнера */
		CMD_CRYPTOCONT_DEL_HEADER			=	0x34,    /*!<Команда  - удаление заголовка криптоконтейнера */
		CMD_CRYPTOCONT_ENCRYPT				=	0x39,    /*!<Команда  - шифрование сектора криптоконтейнера (криптодиска) */
        CMD_CRYPTOCONT_DECRYPT				=	0x40,    /*!<Команда  - расшифрование сектора криптоконтейнера (криптодиска) */
		CMD_CRYPTOCONT_UNMOUNT				=	0x38,    /*!<Команда  - размонтирование выбранного криптоконтейнера (криптодиска) */
		CMD_SET_CIPHER_MODE					=	0x1F,
		CMD_ENCRYPT_DATA					=	0x05,
		CMD_DECRYPT_DATA					=	0x06,
		CMD_ERROR							=	0x1A,
		CMD_READ_DSP_SECTOR					=	0x18,
		CMD_WRITE_DSP_SECTOR				=	0x19,
		CMD_CRYPT_INFO						=	0x1C,
		CMD_SERIAL_NUMBER					=	0x0E,
		CMD_ENC_DEC_PACKET_SEQUENCE_ERROR	=	0x25,
		CMD_REQ_BINDINGS_TABLE				=	0x21,
		CMD_REQ_SERVER_ADDRESS				=	0x1E
    };
	enum
	{
		KEY_SMS	=	0x0000,
		KEY_MMS	=	0x0001,
		KEY_EMAIL	=	0x0002,
		KEY_CHAT	=	0x0003,
		KEY_NOTE	=	0x0004
	};
    enum
    {
        TIMEOUT_ERROR   =   0xFFFF
    };
    void addCRC8(QByteArray *arr);
    void addCRC16(QByteArray *arr);
    bool checkCRC16(const QByteArray &arr);
    bool checkCRC8(const QByteArray &arr);

    bool checkCRC16(const char *data, int len);
    bool checkCRC8(const char *data, int len);
}

#endif // CRYPTEXLINGVO_H
