#include <cstdlib>
#include "device.h"
#include <QInputDialog>
#include <QtCore>
#include <QMessageBox>
#define our_device_vendor_id 65535
#define our_device_product_id 46
#define our_device_transfer_interface 1
#define our_device_transfer_endpoint 2

bool Device::checkSudo() const
{
    QByteArray command;
    command += "echo | sudo -S ls";
    QProcess proc;
    proc.start("bash", QStringList() << "-c" << command);
    proc.waitForFinished(1000);
    QByteArray responseError = proc.readAllStandardError();
    return responseError.isEmpty() || responseError == "Password:";
}

Device::Device()
    : device_handle_(NULL), ctx_(NULL) {
    if(!checkSudo())
    {
        bool ok;
        m_password = QInputDialog::getText(0, QObject::tr("Требуется пароль"), QObject::tr("Пожалуйста, введите пароль пользователя, чтобы программа могла настроить USB драйвер"), QLineEdit::Password, "", &ok).toLatin1();

        if(!ok)
            exit(0);
    }
    QByteArray command;
    command += "echo " + m_password + " | sudo -S ";
    command += "kextunload /System/Library/Extensions/IOUSBFamily.kext/Contents/PlugIns/AppleUSBCDCACMData.kext;";
    command += "echo " + m_password + " | sudo -S ";
    command += "kextunload /System/Library/Extensions/IOUSBFamily.kext/Contents/PlugIns/AppleUSBCDCACMControl.kext";
    QProcess proc;
    proc.start("bash", QStringList() << "-c" << command);
    proc.waitForFinished(5000);
    QByteArray responseError = proc.readAllStandardError();
    if(!responseError.isEmpty() && responseError != "Password:" && !responseError.contains("not found for unload request"))
    {
        QMessageBox::warning(0, QObject::tr("Ошибка настройки драйвера"), QObject::tr("При настройке драйвера произошла ошибка. Программа не будет запущена."));

        exit(0);
    }

    if (!ctx_) {
        if (libusb_init(&ctx_) < 0) { /// init library
            ctx_ = NULL;
            QMessageBox::warning(0, QObject::tr("Ошибка настройки драйвера"), QObject::tr("При настройке драйвера произошла ошибка. Программа не будет запущена."));
            exit(0);
        }
    }
}

Device::~Device() {
    if (ctx_) {
      libusb_exit(ctx_);                              /// close ctx
    }
  QByteArray command;
  command += "echo " + m_password + " | sudo -S ";
  command += "kextload /System/Library/Extensions/IOUSBFamily.kext/Contents/PlugIns/AppleUSBCDCACMData.kext;";
  command += "echo " + m_password + " | sudo -S ";
  command += "kextload /System/Library/Extensions/IOUSBFamily.kext/Contents/PlugIns/AppleUSBCDCACMControl.kext";
  std::system(command.data());
}

bool Device::open(const QString &systemPath) {
    Q_UNUSED(systemPath);

    device_handle_ = libusb_open_device_with_vid_pid(ctx_, our_device_vendor_id,
                                         our_device_product_id);
    if (device_handle_ == NULL) {
        return false;
    }

    libusb_set_configuration(device_handle_, 0);
    if (libusb_claim_interface(device_handle_, our_device_transfer_interface) != 0) { /// replace default drivers
        return false;
    }
    return true;
}

bool Device::close() {

    int r; /// for return values
    r = libusb_release_interface(
      device_handle_, our_device_transfer_interface); /// release claimed
                                                    /// inteface
    libusb_close(device_handle_);                   /// close device
    return !r;
}

int Device::read(char *buffer, int maxSize) {
    /// dont try to change transfer type!!!
    int actual = 0; // bytes trasfered
    auto r = libusb_bulk_transfer(
        device_handle_, (our_device_transfer_endpoint | LIBUSB_ENDPOINT_IN),
        reinterpret_cast<unsigned char *>(buffer), maxSize, &actual,
        10); /// 10 ms to trasfer
    if (r != 0) {
        return -1;
    }
    return actual;
}

int Device::write(char *buffer, const int size) {
    /// dont try to change transfer type!!!
    int actual = 0; // bytes trasfered
    auto r = libusb_interrupt_transfer(
        device_handle_, (our_device_transfer_endpoint | LIBUSB_ENDPOINT_OUT),
        reinterpret_cast<unsigned char *>(buffer), size, &actual,
        5); /// 5 ms to trasfer
    if (r != 0) {
        return -1;
    }
    return actual;
}

QStringList Device::getAvailablePorts(quint32 *errorCode) {
    Q_UNUSED(errorCode);
    QStringList result_usb;
    libusb_device **devs;
    libusb_context *ctx = NULL;
    int r;
    ssize_t cnt;
    r = libusb_init(&ctx);
    if(r < 0) {
        return result_usb;
    }
    libusb_set_debug(ctx, 0);
    cnt = libusb_get_device_list(ctx, &devs);
    if(cnt < 0) {
        return result_usb;
    }
    for(auto i = 0; i < cnt; i++) {
        libusb_device_descriptor desc;
        r = libusb_get_device_descriptor(devs[i], &desc);
        if (r >= 0) {
            if(desc.idVendor == our_device_vendor_id && desc.idProduct == our_device_product_id)
            result_usb << QString::number(desc.idVendor) + "|" + QString::number(desc.idProduct);
        }
    }
    libusb_free_device_list(devs, 1);
    libusb_exit(ctx);
    return result_usb;
}


/*
void printdev(libusb_device *dev); //prototype of the function
using namespace std;
int print_dev() {
    libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
    libusb_context *ctx = NULL; //a libusb session
    int r; //for return values
    ssize_t cnt; //holding number of devices in list
    r = libusb_init(&ctx); //initialize a library session
    if(r < 0) {
        cout<<"Init Error "<<r<<endl; //there was an error
                return 1;
    }
    libusb_set_debug(ctx, 0); //set verbosity level to 3, as suggested in the documentation
    cnt = libusb_get_device_list(ctx, &devs); //get the list of devices
    if(cnt < 0) {
        cout<<"Get Device Error"<<endl; //there was an error
    }
    cout<<cnt<<" Devices in list."<<endl; //print total number of usb devices
        ssize_t i; //for iterating through the list
    for(i = 0; i < cnt; i++) {
                printdev(devs[i]); //print specs of this device
        }
        libusb_free_device_list(devs, 1); //free the list, unref the devices in it
        libusb_exit(ctx); //close the session
        return 0;
}

void printdev(libusb_device *dev) {
    libusb_device_descriptor desc;
    int r = libusb_get_device_descriptor(dev, &desc);
    if (r < 0) {
        cout<<"failed to get device descriptor"<<endl;
        return;
    }
    cout<<"Number of possible configurations: "<<(int)desc.bNumConfigurations<<"  ";
    cout<<"Device Class: "<<(int)desc.bDeviceClass<<"  ";
    cout<<"VendorID: "<<desc.idVendor<<"  ";
    cout<<"ProductID: "<<desc.idProduct<<endl;
    libusb_config_descriptor *config;

    libusb_get_config_descriptor(dev, 0, &config);

    cout<<"Interfaces: "<<(int)config->bNumInterfaces<<" ||| ";

    const libusb_interface *inter;

    const libusb_interface_descriptor *interdesc;

    const libusb_endpoint_descriptor *epdesc;

    for(int i=0; i<(int)config->bNumInterfaces; i++) {

        inter = &config->interface[i];

        cout<<"Number of alternate settings: "<<inter->num_altsetting<<" | ";

        for(int j=0; j<inter->num_altsetting; j++) {

            interdesc = &inter->altsetting[j];

            cout<<"Interface Number: "<<(int)interdesc->bInterfaceNumber<<" | ";

            cout<<"Number of endpoints: "<<(int)interdesc->bNumEndpoints<<" | ";

            for(int k=0; k<(int)interdesc->bNumEndpoints; k++) {

                epdesc = &interdesc->endpoint[k];


                cout<<"Descriptor Type: "<<(int)epdesc->bDescriptorType<<" | ";

                cout<<"EP Address: "<<(int)epdesc->bEndpointAddress<<" | ";

            }

        }

    }
    cout<<endl<<endl<<endl;

    libusb_free_config_descriptor(config);

}

*/
