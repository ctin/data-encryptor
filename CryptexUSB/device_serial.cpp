#include "device.h"

Device::Device()
{

}

Device::~Device()
{
}

bool Device::open(const QString &systemPath)
{
    m_port.setPortName(systemPath);
    return m_port.open(QIODevice::ReadWrite);
}

bool Device::close()
{
    m_port.close();
    return true;
}

int Device::read(char *buffer, int maxSize)
{
    m_port.waitForReadyRead(1);
    return m_port.read(buffer, maxSize);
}

int Device::write(char *buffer, const int size)
{
    int written = m_port.write(buffer, size);
    m_port.waitForBytesWritten(100);
    return written;
}

QStringList Device::getAvailablePorts(quint32 *errorCode)
{
    QStringList ports;
    foreach(QSerialPortInfo info, QSerialPortInfo::availablePorts())
    {
        ports << info.systemLocation();
    }
    return ports;
}
