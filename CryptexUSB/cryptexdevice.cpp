#include "cryptexdevice.h"
#include <QThread>
using namespace  std;

CryptexDevice::CryptexDevice(QString systemPathArg, QObject *parent)
: QObject(parent)
{
    resetReadBuffer();
    resetWriteBuffer();
    resetIsOpen();
    resetPacketNum();
    resetWaitTime();
    setAvailable(false);
    setSystemPath(systemPathArg);
}

CryptexDevice::~CryptexDevice()
{
    close();
}

bool CryptexDevice::open(QString arg)
{
    if(arg.isEmpty())
        return false;
    if(systemPath() != arg && isOpen())
        close();
    setSystemPath(arg);
    return open();
}

bool CryptexDevice::open()
{
    if(isOpen())
        return true;
    if(systemPath().isEmpty())
        return false;
    bool result = m_device.open(systemPath());
    setIsOpen(result);
    //qDebug() << systemPath() << "opened: " << result;
    return result;
}


bool CryptexDevice::close()
{
    if(!isOpen())
        return true;
    m_device.close();
    setIsOpen(false);
    //qDebug() << "closed";
    return true;
}

bool CryptexDevice::write(QByteArray arr)
{
    if(!isOpen())
        return false;
    int result = m_device.write(arr.data(), arr.size());
    if(result < 0)
    {
        //qDebug() << "write failed";
        close();
    }
    //qDebug() << "written" << result;
    return result == arr.size();
}

bool CryptexDevice::read(QByteArray *arr)
{
    if(!isOpen())
        return false;
    static char buffer[INTERNAL_BUFFER_SIZE];
    int read = m_device.read(buffer, INTERNAL_BUFFER_SIZE);
    if(read >= 0)
        *arr += QByteArray(buffer, read);
    else
    {
        //qDebug() << "read failed" << GetLastError();
        close();
    }
    //qDebug() << "read" << read;
    return read >= 0;

}

CryptexDevice::Result CryptexDevice::transaction(const quint8 &command, quint16 *param, QByteArray *data)
{
    Result result = _transaction(command, param, data);
    setAvailable(result);
    return result;
}

CryptexDevice::Result CryptexDevice::_transaction(const quint8 &command, quint16 *param, QByteArray *data)
{
    quint16 tmpVal = 0;
    if(!param)
        param = &tmpVal;
    QByteArray tmpData;
    if (!data)
        data = &tmpData;
    CryptexDevice::Result result;
    const bool wasOpen = isOpen();
    if(!wasOpen)
        if(!open())
            return result.setError(Result::OpenError);
    if(!write(Lingvo::createPacket(command, *param, *data)))
        return result.setError(Result::WriteError);
    data->clear();
    quint32 elaplsed = 0;
    bool checked = false;
    if(result)
    {
        static QTime time;
        time.restart();
        do {
            qApp->processEvents();
            if(!read(data))
                return result.setError(Result::ReadError);
            elaplsed = time.elapsed();
            checked = Lingvo::adjustAndCheck(data, command);
        } while(result && !checked && elaplsed < waitTime());
    }
    if(!wasOpen && result)
        if(!close())
            return result.setError(Result::CloseError);
    if(!checked && elaplsed >= waitTime())
        return result.setError(Result::TimeoutError); // timeout error

    if(Lingvo::getCommand(*data) == Lingvo::CMD_ERROR)
    {
        data->clear();
        *param = Lingvo::getParam(*data);
        return result.setError(Result::LingvoError);
    }
    *param = Lingvo::getParam(*data);
    *data = Lingvo::getData(*data);
    return result;
}

CryptexDevice::Result CryptexDevice::transaction(const quint8 &command, const quint16 &param, QByteArray *data)
{
    quint8 tmpCommand = command;
    quint16 tmpParam = param;
    return transaction(tmpCommand, &tmpParam, data);
}

CryptexDevice::Result CryptexDevice::transaction(const quint8 &command, QByteArray *data)
{
    quint8 tmpCommand = command;
    quint16 tmpParam = 0;
    return transaction(tmpCommand, &tmpParam, data);
}

QString CryptexDevice::systemPath() const
{
    return m_systemPath;
}

QByteArray CryptexDevice::readBuffer() const
{
    return m_readBuffer;
}

QByteArray CryptexDevice::writeBuffer() const
{
    return m_writeBuffer;
}

bool CryptexDevice::isOpen() const
{
    return m_isOpen;
}

quint8 CryptexDevice::packetNum() const
{
    return m_packetNum;
}

quint32 CryptexDevice::waitTime() const
{
    return m_waitTime;
}

bool CryptexDevice::available() const
{
    return m_available;
}

void CryptexDevice::setSystemPath(QString arg)
{
    if (m_systemPath != arg) {
        m_systemPath = arg;
        emit systemPathChanged(arg);
    }
}

void CryptexDevice::setReadBuffer(QByteArray arg)
{
    if (m_readBuffer != arg) {
        m_readBuffer = arg;
        emit readBufferChanged(arg);
    }
}

void CryptexDevice::setWriteBuffer(QByteArray arg)
{
    if (m_writeBuffer != arg) {
        m_writeBuffer = arg;
        emit writeBufferChanged(arg);
    }
}

void CryptexDevice::setIsOpen(bool arg)
{
    if (m_isOpen != arg) {
        m_isOpen = arg;
        emit isOpenChanged(arg);
    }
}

void CryptexDevice::setPacketNum(quint8 arg)
{
    if (m_packetNum != arg) {
        m_packetNum = arg;
        emit packetNumChanged(arg);
    }
}

void CryptexDevice::setWaitTime(quint32 arg)
{
    if (m_waitTime != arg) {
        m_waitTime = arg;
        emit waitTimeChanged(arg);
    }
}

void CryptexDevice::resetSystemPath()
{
    setSystemPath("");
}

void CryptexDevice::resetReadBuffer()
{
    setReadBuffer(QByteArray());
}

void CryptexDevice::resetWriteBuffer()
{
    setWriteBuffer(QByteArray());
}

void CryptexDevice::resetIsOpen()
{
    setIsOpen(false);
}

void CryptexDevice::resetPacketNum()
{
    setPacketNum(0);
}

void CryptexDevice::resetWaitTime()
{
    setWaitTime(IDEAL_WAIT_TIME);
}

void CryptexDevice::setAvailable(bool arg)
{
    if (m_available == arg)
        return;

    m_available = arg;
    emit availableChanged(arg);
}

CryptexDevice::Result::Result()
{
    setError(Ok);
}

CryptexDevice::Result::operator bool() const
{
    return m_error == Ok;
}

CryptexDevice::Result& CryptexDevice::Result::setError(CryptexDevice::Result::Error err)
{
    m_error = err;
    return *this;
}

QString CryptexDevice::Result::errorMessage()
{
    return QString(); //TODO: добавить сообщения
}

