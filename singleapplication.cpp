// "single_application.cpp"

#include <QTimer>
#include <QByteArray>
#include <QFile>

#include "singleapplication.h"

SingleApplication::SingleApplication(const QString uniqueKey) : QObject()
{
    m_sharedMemory.setKey(uniqueKey);

    // when  can create it only if it doesn't exist
    if (m_sharedMemory.create(maxBufferSize()))
    {
        m_sharedMemory.lock();
        static_cast<char*>(m_sharedMemory.data())[0] = '\0';
        m_sharedMemory.unlock();

        m_alreadyExists = false;

        // start checking for messages of other instances.
        QTimer *pTimer = new QTimer(this);
        connect(pTimer, SIGNAL(timeout()), this, SLOT(checkForMessage()));
        pTimer->start(200);
    }
    // it exits, so we can attach it?!
    else if (m_sharedMemory.attach()){
        m_alreadyExists = true;
    }
    else{
        // error
    }

}

int SingleApplication::maxBufferSize()
{
    return 0xffff;  //it must be maximum file name length, but who cares;
}

// public slots.

void SingleApplication::checkForMessage()
{
    QStringList arguments;

    m_sharedMemory.lock();
    char *from = static_cast<char*>(m_sharedMemory.data());

    while(*from != '\0'){
        unsigned char sizeToRead = *from;
        ++from;

        QByteArray byteArray = QByteArray(from, sizeToRead);
        byteArray.append('\0');
        from += sizeToRead;

        arguments << QString::fromUtf8(byteArray.constData());
    }

    static_cast<char*>(m_sharedMemory.data())[0] = '\0';
    m_sharedMemory.unlock();

    if(arguments.size())
        emit messageAvailable( arguments );
}

// public functions.

bool SingleApplication::sendMessage(const QString &message)
{
    //we cannot send mess if we are master process!
    if (isMasterApp()){
        return false;
    }
    if(message.startsWith("--") || !QFile::exists(message))
        return false;

    QByteArray byteArray;
    byteArray.append(message.toUtf8());
    byteArray.prepend(byteArray.size());
    byteArray.append('\0');
    if(byteArray.size() > maxBufferSize())
    {
        int size= byteArray.size();
        byteArray.clear();
        byteArray.append(QString("data more than buffer size. Data size: $%1").arg(size));
    }

    m_sharedMemory.lock();
    char *to = static_cast<char*>(m_sharedMemory.data());
    while(*to != '\0'){
        int sizeToRead = int(*to);
        to += sizeToRead + 1;
    }

    const char *from = byteArray.data();
    memcpy(to, from, qMin(m_sharedMemory.size(), byteArray.size()));
    m_sharedMemory.unlock();

    return true;
}
