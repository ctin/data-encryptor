#ifndef SHELLEXTENSIONMANAGER_H
#define SHELLEXTENSIONMANAGER_H

#include <QObject>

class ShellExtensionManager : public QObject
{
    Q_OBJECT
public:
    explicit ShellExtensionManager(QObject *parent = 0);
    static bool checkAdminRights();
signals:

public slots:
    void registerShellExtension();
    void unregisterShellExtension();
};

#endif // SHELLEXTENSIONMANAGER_H
