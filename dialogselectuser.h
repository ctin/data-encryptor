#ifndef DIALOGSELECTUSER_H
#define DIALOGSELECTUSER_H

#include "ui_dialogselectuser.h"

class DialogSelectUser : public QDialog, public Ui::DialogSelectUser
{
    Q_OBJECT

public:
    explicit DialogSelectUser(QStringList names = QStringList(), QWidget *parent = 0);
    void setNames(const QStringList &names);
private slots:
    void on_lineEdit_textChanged(const QString &arg1);
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_listWidget_currentRowChanged(int currentRow);

private:
    QStringList m_names;

};

#endif // DIALOGSELECTUSER_H
