#ifndef KEYCATCHERS_H
#define KEYCATCHERS_H
#include <QObject>
#include <QEvent>
#include <QKeyEvent>

class TabKeyCatcher : public QObject
{
    Q_OBJECT
public:
    TabKeyCatcher(QObject *parent = 0) : QObject(parent)
        { }
signals:
    void tabPressed();
    void backtabPressed();
protected:
    bool eventFilter(QObject *obj, QEvent *event)
    {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Tab) {
                if(keyEvent->modifiers() == Qt::ShiftModifier)
                    emit backtabPressed();
                else
                    emit tabPressed();
                return true;
            }
            if(keyEvent->key() == Qt::Key_Backtab)
            {
                emit backtabPressed();
                return true;
            }
        }
        return QObject::eventFilter(obj, event);
    }
};

class DelTabKeyCatcher : public TabKeyCatcher
{
    Q_OBJECT
public:
    DelTabKeyCatcher(QObject *parent = 0) : TabKeyCatcher(parent)
    {}
signals:
    void delPressed();
protected:
    bool eventFilter(QObject *obj, QEvent *event)
    {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Delete)
            {
                emit delPressed();
                return true;
            }
        }
        return TabKeyCatcher::eventFilter(obj, event);
    }
};

class DownTabKeyCatcher : public TabKeyCatcher
{
    Q_OBJECT
public:
    DownTabKeyCatcher(QObject *parent = 0) : TabKeyCatcher(parent)
    {}
signals:
    void downPressed();
protected:
    bool eventFilter(QObject *obj, QEvent *event)
    {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Down)
            {
                emit downPressed();
                return true;
            }
        }
        return TabKeyCatcher::eventFilter(obj, event);
    }
};


#endif // KEYCATCHERS_H
