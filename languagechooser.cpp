#include "languagechooser.h"
#include <QDir>
#include <QTranslator>
#include <QApplication>
#include <QSettings>

static const QString defaultLangKey = "defaultLangKey";


LanguageChooser::LanguageChooser(QObject *parent) :
    QObject(parent)
{
}

void LanguageChooser::loadDefLang()
{
    QSettings settings;
    QString defLang = settings.value(defaultLangKey, "").toString();
    if(defLang.isEmpty())
    {
        const QStringList &defLangs = QLocale::system().uiLanguages();
        if(!defLangs.isEmpty())
            defLang = defLangs.first();
    }
    setLanguage(defLang);
}

void LanguageChooser::saveDefLang()
{
    QSettings settings;
    settings.setValue(defaultLangKey, language());
}

void LanguageChooser::switchTranslator(QTranslator* translator, const QString& filename)
{
    qApp->removeTranslator(translator);
    if(translator->load(filename))
    {
        qApp->installTranslator(translator);
    }
}

QString LanguageChooser::language() const
{
    return m_language;
}

void LanguageChooser::setLanguage(QString arg)
{
    if (m_language == arg)
        return;

    m_language = arg;
    loadLanguage(arg);
    emit languageChanged(arg);
}

void LanguageChooser::loadLanguage(const QString& rLanguage)
{
    QLocale locale = QLocale(rLanguage);
    QLocale::setDefault(locale);
    switchTranslator(&m_translator, QString(":/translations/filetextcryptor_%1.qm").arg(rLanguage.left(2)));
    switchTranslator(&m_translatorQt, QString(":/translations/qtbase_%1.qm").arg(rLanguage.left(2)));
}
