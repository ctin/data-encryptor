#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QTimer>
#include <QAbstractNativeEventFilter>

class Application : public QApplication
{
    Q_OBJECT
public:
    Application(int &argc, char *argv[]);
    void onDeviceChanged(int ms);
public slots:
signals:
    void deviceChanged();
protected:
    bool winEventFilter (MSG *message, long *result);
private:
    QTimer m_portChangedTimer;


};

class AbstractNativeEventFilter : public QAbstractNativeEventFilter
{
public:
    AbstractNativeEventFilter(Application *pApp);
    bool nativeEventFilter(const QByteArray &eventType, void *message, long *result);
    Application *m_pApplication;
};

#endif // APPLICATION_H
