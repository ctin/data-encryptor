#include "dialogselectuser.h"
#include <QCompleter>

DialogSelectUser::DialogSelectUser(QStringList names, QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    setLayout(verticalLayout);
    setWindowFlags(Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    setNames(names);

    connect(listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(accept()));
}

void DialogSelectUser::setNames(const QStringList &names)
{
    m_names = names;
    listWidget->clear();
    listWidget->addItems(names);
    QCompleter *pCompleter = new QCompleter(names);
    pCompleter->setFilterMode(Qt::MatchContains);
    //pCompleter->setCompletionMode(QCompleter::InlineCompletion);

    pCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    lineEdit->setCompleter(pCompleter);
}

void DialogSelectUser::on_lineEdit_textChanged(const QString &arg1)
{
    QString regText = arg1;
    regText.prepend(".*").append(".*");
    QRegExp reg(arg1, Qt::CaseInsensitive);
    listWidget->clear();
    listWidget->addItems(m_names.filter(reg));

    if(listWidget->count())
    {
        listWidget->setCurrentRow(0);
    }
}

void DialogSelectUser::on_listWidget_itemClicked(QListWidgetItem *pItem)
{
    if(pItem)
        lineEdit->setText(pItem->text());
}

void DialogSelectUser::on_listWidget_currentRowChanged(int currentRow)
{
    pushButton->setEnabled(currentRow >= 0);
}
