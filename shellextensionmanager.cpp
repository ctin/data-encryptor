#include "shellextensionmanager.h"
#include <QSettings>
#include <QApplication>
#include <QDir>

ShellExtensionManager::ShellExtensionManager(QObject *parent) :
    QObject(parent)
{
#ifdef Q_OS_WIN
    {
        QSettings settings("HKEY_CLASSES_ROOT\\*\\shell\\", QSettings::NativeFormat);
        if(settings.allKeys().contains(QApplication::applicationName() + "/."))
        {
            settings.beginGroup(QApplication::applicationName());
            if(settings.allKeys().contains("command/."))
            {
                settings.beginGroup("command");
                QString currentVal = settings.value(".", "").toString();
                QString path = QDir::toNativeSeparators(QApplication::applicationFilePath());
                if(!currentVal.isEmpty() && !currentVal.startsWith(path))
                    settings.setValue("Default", path + " \"%1\"");
            }
        }
    }
    {
        QSettings settings("HKEY_CLASSES_ROOT\\Directory\\shell\\", QSettings::NativeFormat);
        if(settings.allKeys().contains(QApplication::applicationName() + "/."))
        {
            settings.beginGroup(QApplication::applicationName());
            if(settings.allKeys().contains("command/."))
            {
                settings.beginGroup("command");
                QString currentVal = settings.value(".", "").toString();
                QString path = QDir::toNativeSeparators(QApplication::applicationFilePath());
                if(!currentVal.isEmpty() && !currentVal.startsWith(path))
                    settings.setValue("Default", path + " \"%1\"");
            }
        }
    }
#endif //Q_OS_WIN
}

bool ShellExtensionManager::checkAdminRights()
{
    QString str = QApplication::applicationName() + "test";
    {
        QSettings settings("HKEY_CLASSES_ROOT\\*\\shell\\", QSettings::NativeFormat);
        settings.remove(str);
    }
    {
        QSettings settings("HKEY_CLASSES_ROOT\\*\\shell\\" + str, QSettings::NativeFormat);
        settings.setValue("Default", QString("&") + tr("Использовать шифратор"));
    }
    bool result;
    QSettings settings("HKEY_CLASSES_ROOT\\*\\shell\\", QSettings::NativeFormat);
    result = settings.allKeys().contains(str + "/.");
    settings.remove(str);
    return result;
}

void ShellExtensionManager::registerShellExtension()
{
#ifdef Q_OS_WIN
    {
        QSettings settings("HKEY_CLASSES_ROOT\\*\\shell\\" + QApplication::applicationName(), QSettings::NativeFormat);
        settings.setValue("Default", QString("&") + tr("Использовать шифратор"));
        //settings.setValue("Icon", "shell32.dll,194");
        settings.setValue("MultiSelectModel", "Single");
        settings.beginGroup("command");
        QString path = QDir::toNativeSeparators(QApplication::applicationFilePath());
        settings.setValue("Default", path + " \"%1\"");
    }
    {
        QSettings settings("HKEY_CLASSES_ROOT\\Directory\\shell\\" + QApplication::applicationName(), QSettings::NativeFormat);
        settings.setValue("Default", QString("&") + tr("Использовать шифратор"));
        //settings.setValue("Icon", "shell32.dll,194");
        settings.setValue("MultiSelectModel", "Single");
        settings.beginGroup("command");
        QString path = QDir::toNativeSeparators(QApplication::applicationFilePath());
        settings.setValue("Default", path + " \"%1\"");
    }
#endif //Q_OS_WIN
}

void ShellExtensionManager::unregisterShellExtension()
{
#ifdef Q_OS_WIN
    {
        QSettings settings("HKEY_CLASSES_ROOT\\*\\shell", QSettings::NativeFormat);
        settings.remove(QApplication::applicationName());
    }
    {
        QSettings settings("HKEY_CLASSES_ROOT\\Directory\\shell", QSettings::NativeFormat);
        settings.remove(QApplication::applicationName());
    }
#endif //Q_OS_WIN
}
