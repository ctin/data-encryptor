 #-------------------------------------------------
#
# Project created by QtCreator 2014-09-17T17:23:49
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11
CONFIG += LIBUSB #for libusb !win32
#CONFIG += SERIAL #for all systems

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FileTextCryptor

TEMPLATE = app

DEFINES += TARGET=\\\"$$TARGET\\\"

SOURCES += main.cpp\
        mainwindow.cpp \
    core.cpp \
    dialogselectuser.cpp \
    fileworker.cpp \
    shellextensionmanager.cpp \
    languagechooser.cpp \
    dialogabout.cpp

HEADERS  += mainwindow.h \
    core.h \
    dialogselectuser.h \
    fileworker.h \
    shellextensionmanager.h \
    languagechooser.h \
    dialogabout.h \
    version.h \
    keyCatchers.h

FORMS    += mainwindow.ui \
    dialogselectuser.ui \
    dialogabout.ui

RESOURCES += \
    res.qrc

include(CryptexUSB/cryptex.pri)


win32 {
    RC_FILE += Images/myicon.rc
    HEADERS += application.h \
        singleapplication.h
    SOURCES += application.cpp \
        singleapplication.cpp
}

mac {
    QMAKE_MAC_SDK = macosx10.9
    ICON = Images/FTC.icns
}

TRANSLATIONS += translations/filetextcryptor_ru.ts \
                translations/filetextcryptor_en.ts

OTHER_FILES +=
