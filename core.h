#ifndef CORE_H
#define CORE_H

#include <QObject>
#include <QSignalMapper>
#include "mainwindow.h"
#include "CryptexUSB/allthisthings.h"
#include "fileworker.h"
#include "shellextensionmanager.h"
#include "languagechooser.h"

class Core : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentName READ currentName WRITE setCurrentName NOTIFY currentNameChanged)
public:
    explicit Core(QObject *parent = 0);

private:
    struct Result {
        Result();
        enum Error {
            Ok,
            DeeperError,
            Skipped,
            NoSuchInFile,
            CanNotOpenInFile,
            CanNotCreatePath,
            CanNotOpenOutFile,
            LoginNotFoundForText,
            LoginNotFoundForFile,
            SenderUnknown,
            WrongCryptedDataFormat,
            HashError,
            TextIsTooLarge,
            ReceiverEmpty,
            ReceiverIncorrect,
            myLoginError, //ошибка на грани возможного

        };
        Error m_error;
        QString m_deeperErrorMessage;
        QString errorMessage();
        operator bool() const;
        Result& setDeeperError(QString deeperErrorMessage);
        Result& setError(Error err);
    };
    AllThisThings m_device;
    MainWindow m_mainWindow;
    ShellExtensionManager m_shellExtensionManager;
    LanguageChooser *m_pLanguageChooser;
public slots:
    bool findDeviceAndRefreshUsers();
    void checkAvailable();
    void addFile();
    void addDir();
    void addFile(QString);


    void removeSelectedFilesFromTable();
    void removeSelectedFilesFromTable(int pos);
    void removeSelectedFilesFromTable(QString);

    void clearTable();

    void decryptAllFiles();
    void encryptAllFiles();
    void receiveMessageFromOtherInstance(QStringList);

private slots:
    void test();
    void onTranslate();
    void recryptInputText();
    void onNameChanged(QString arg);
    void onTableCellDoubleClicked(int row, int column);
    void onOriginalTextChanged();
    void updateControlsEnable();

    void translateTable(int pos = -1);
    void onDeviceAvailableChanged(bool);
    void onMyLoginChanged(QByteArray arg);
    void onTableSelectionChanged();

private:
    QTimer m_checkTimer;
    bool startFile(QString inFileName, QDataStream *pOutStream);

    QMap<QString, QByteArray> m_userlist;
    Core::Result encryptText(QByteArray &in, QString *out);
    Core::Result decryptText(QByteArray &in, QString *out);
    Result encryptFile(QProgressDialog *pProgress, QString prefix, QString filename, QDataStream *pOutStream);
    Result decryptFile(QString currentPath, QDataStream *pInStream);
    const QByteArray m_senderNamePrefix, m_receiverNamePrefix;
    QByteArray getReceiverLogin();
    QList<FileWorker *> m_fileList;
    QTimer *m_pTimerTextEdit;
    void showInGraphicalShell(const QString &pathIn);

public:
    Result prepareDecryptFile(FileWorker *pWorker);
    Result prepareEncryptFileFolder(QProgressDialog *pDialog, QDataStream *out, FileWorker *pWorker);
    void translateTableRow(int i);
    void refreshSenderName(FileWorker *pWorker);
public:
    QString currentName() const;
signals:
    void currentNameChanged(QString arg);
public slots:
    void setCurrentName(QString arg);
private:
    QString m_currentName;
};

#endif // CORE_H
