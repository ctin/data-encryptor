#ifndef FILEWORKER_H
#define FILEWORKER_H

#include <QtCore>
#include <QtWidgets>

class FileWorker : public QObject
{
    Q_OBJECT
public:
    enum Status {
        CREATED,
        SUCCESS,
        HAS_ERRORS
    };

    Q_PROPERTY(QString inFileName READ inFileName WRITE setInFileName NOTIFY inFileNameChanged)
    Q_PROPERTY(QString outFileName READ outFileName WRITE setOutFileName NOTIFY outFileNameChanged)
    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(QString lastError READ lastError WRITE setLastError NOTIFY lastErrorChanged)
    Q_PROPERTY(int pos READ pos WRITE setPos NOTIFY posChanged)
    Q_PROPERTY(QString sender READ sender WRITE setSender NOTIFY senderChanged)
    Q_PROPERTY(QByteArray receiverLogin READ receiverLogin WRITE setReceiverLogin NOTIFY receiverLoginChanged)
    Q_PROPERTY(QByteArray senderLogin READ senderLogin WRITE setSenderLogin NOTIFY senderLoginChanged)
    Q_PROPERTY(Status completed READ completed WRITE setCompleted NOTIFY completedChanged)
    Q_PROPERTY(QString errorReason READ errorReason WRITE setErrorReason NOTIFY errorReasonChanged)
public:

    explicit FileWorker(int pos, QString inFileName, QObject *parent = 0);
    static QString cryptedExtension();
private slots:
    void refreshCompleted();
public:
    QString inFileName() const;
    QString outFileName() const;
    int progress() const;
    QString lastError() const;
    QLabel *getLabelStatus();
    int pos() const;
    QString sender() const;
    static bool encrypt(const QString &fileName);
    bool encrypt() const;
    Status completed() const;
    QByteArray receiverLogin() const;
    QByteArray senderLogin() const;
    QString errorReason() const;

public slots:
    void setInFileName(QString arg);
    void setOutFileName(QString arg);
    void setProgress(int arg);
    void setLastError(QString arg);
    void setPos(int arg);
    void setSender(QString arg);
    void setCompleted(Status arg);
    void setReceiverLogin(QByteArray arg);
    void setSenderLogin(QByteArray arg);
    void setErrorReason(QString arg);

private slots:
    void selectOutFile();
signals:
    void inFileNameChanged(QString arg);
    void outFileNameChanged(QString arg);
    void progressChanged(int arg);
    void lastErrorChanged(QString arg);
    void posChanged(int arg);
    void senderChanged(QString arg);
    void completedChanged(Status arg);
    void receiverLoginChanged(QByteArray arg);
    void senderLoginChanged(QByteArray arg);
    void errorReasonChanged(QString arg);

private:
    QString m_inFileName;
    QString m_outFileName;
    QString m_lastError;
    int m_progress;
    int m_pos;
    QString m_sender;
    QLabel *m_pLabelStatus;
    Status m_completed;
    QByteArray m_receiverLogin;
    QByteArray m_senderLogin;
    QString m_errorReason;
};

#endif // FILEWORKER_H
