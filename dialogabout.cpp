#include "dialogabout.h"
#include "version.h"

DialogAbout::DialogAbout(QWidget *parent) :
    QDialog(parent),
    m_buildDate(QLocale(QLocale::C).toDate(QString(__DATE__).simplified(), QLatin1String("MMM d yyyy"))),
    m_buildTime(QTime::fromString(__TIME__))
{
    setupUi(this);
    label_link->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard | Qt::LinksAccessibleByMouse);
    label_link->openExternalLinks();
    label_link->setOpenExternalLinks(true);
    label_link->setFocusPolicy(Qt::NoFocus);

    label_mail->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard | Qt::LinksAccessibleByMouse);
    label_mail->openExternalLinks();
    label_mail->setOpenExternalLinks(true);
    label_mail->setFocusPolicy(Qt::NoFocus);

    setWindowFlags((this->windowFlags() & ~Qt::WindowContextHelpButtonHint));
    setLayout(verticalLayout_3);
    setFocusPolicy(Qt::ClickFocus);
    refreshText();
}

void DialogAbout::refreshText()
{
    retranslateUi(this);
    QString version = VERSION_STRING;
    version.replace('-', '.');
    label_version->setText(tr("Версия программы") + QString(": <b>%1</b>").arg(version));

    QString result = tr("Дата изготовления") + ": <b>" + m_buildDate.toString("d-MM-yyyy") + "</b>";
    label_date->setText(result);
    label_time->setText(tr("Время изготовления") + ": <b>" + m_buildTime.toString() + "</b>");
    setFixedSize(sizeHint());

}

void DialogAbout::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    close();
}

void DialogAbout::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    setFocus();
}
