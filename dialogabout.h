#ifndef DIALOGABOUT_H
#define DIALOGABOUT_H

#include "ui_dialogabout.h"
#include <QDate>
#include <QFocusEvent>

class DialogAbout : public QDialog, private Ui::DialogAbout
{
    Q_OBJECT

public:
    explicit DialogAbout(QWidget *parent = 0);
    void refreshText();
private:
    const QDate m_buildDate;
    const QTime m_buildTime;
protected:
    void focusOutEvent(QFocusEvent* event);
    void showEvent(QShowEvent *event);
};

#endif // DIALOGABOUT_H
