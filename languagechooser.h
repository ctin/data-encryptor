#ifndef LANGUAGECHOOSER_H
#define LANGUAGECHOOSER_H

#include <QObject>
#include <QTranslator>

class LanguageChooser : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY languageChanged)
public:
    explicit LanguageChooser(QObject *parent = 0);
    void loadDefLang();
    QStringList findQmFiles();

signals:
public slots:
    void saveDefLang();
private:
    void switchTranslator(QTranslator* translator, const QString& filename);
    void switchTranslator(QTranslator* translator, const QLocale &locale, const QString& filename);

    void loadLanguage(const QString& rLanguage);

    QTranslator m_translator;
    QTranslator m_translatorQt;

public:
    QString language() const;
signals:
    void languageChanged(QString arg);
public slots:
    void setLanguage(QString arg);
private:
    QString m_language;
};

#endif // LANGUAGECHOOSER_H
